
# Le cours:


<div class="centre">
<iframe 
src="./gen_alea/generateur_aleatoire.pdf"
width="1000" height="500" 
frameborder="0" 
allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
</iframe>
</div>


# Les exercices:

[Fiche n°1: QUelques générateurs aléatoires](./gen_alea/exercice_gcm.pdf){ .md-button target="_blank" rel="noopener" }



# Codes Python pour les exercices:

## Générateur à congruence linéaire
```python
a = 65539
c = 0
m = 2**31
graine = 5689
x =(a*graine + c)%m
periode = 1
while x != graine:
    periode += 1
    #print(x)
    x =(a*x + c)%m
print(f"La période de ce générateur est {periode}")
```
## Graphique pour juger de l'uniformité du générateur
```python
import matplotlib.pyplot as plt
liste_abscisse = []
liste_ordonnee = []
a = 65539
c = 0
m = 2**31
graine = 1411678217726

x = graine
y = (a*graine + c)%m

for i in range(200000):
    liste_abscisse.append(x)
    liste_ordonnee.append(y)
    x = y
    y = (a*x + c)%m
    
#print(liste_abscisse)
#print(liste_ordonnee)
plt.scatter(liste_abscisse, liste_ordonnee)
plt.title("GCM")

plt.show()
```

## Et même une petite animation pour juger le générateur pseudo aléatoire de python...

``` python
from random import random
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np

liste_abscisse = []
liste_ordonnee = []
x = random()
y = random()

for i in range(20000):#choix arbitraire, periode trop grande...
    liste_abscisse.append(x)
    liste_ordonnee.append(y)
    x = y
    y = random()

fig = plt.figure()
scat = plt.scatter(liste_abscisse[0], liste_ordonnee[0], c ="b", marker ='x')
plt.xlim(0, 1)
plt.ylim(0, 1)


def update(frame):
    # for each frame, update the data stored on each artist.
    x = liste_abscisse[:frame]
    y = liste_ordonnee[:frame]    
    data = np.stack([x, y]).T
    scat.set_offsets(data)
    return scat,


ani = animation.FuncAnimation(fig=fig, func=update, frames=4000, interval=1)
plt.show()
```