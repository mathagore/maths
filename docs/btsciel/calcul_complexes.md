# Manipuler des nombres complexes avec Python

Nous allons faire des calculs avec les nombres complexes. Ce sera facile car Python sait les manipuler.




!!! tip "La méthode générale!"
    Avec Python, tu manipules les nombres complexes comme les autres nombres. La notation pour le nombre complexe $i$ (qui vérifie $i^2 = -1$) est le symbole ``j`` (plus exactement ``1j``).

Par exemple, le nombre complexe $4-3i$ se note ``4-3j``. 

Les opérations classiques s'écrivent comme d'habitude: par exemple le calcul de $(1+2i)(4-i)$ s'écrit ``(1+2j)*(4-1j)`` qui donne  ``6+7j`` en python.
<center>

| Opérations mathématiques     | Opérateurs Python                         |
| ----------- | ------------------------------------ |
| $z_1+z_2$       | ``z1+z2``  |
| $z_1\times z_2$   | ``z1*z2`` |
| $z^n$    | ``z**n`` |
| Partie réelle de $z=a+ib$    | ``z.real`` |
| Partie imaginaire de $z=a+ib$    | ``z.imag`` |
| Module de $z=a+ib$    | ``abs(z)`` |
| Conjugué $\bar{z}$ de $z=a+ib$    | ``z.conjugate()`` |

</center>


!!! exercice "À vous de jouer"
    1. Définis les nombres complexes $z_1=1+2i$ et $z_2=3-i$.
    2. Calcule $z_1+z_2$, $z_1\times z_2$, $z_1^2$, $|z_1|$ et $\dfrac {z_1}{z_2}$.
    3. Définis le nombre complexe $z=(3-4i)^2(2+i)$ et fais afficher sa partie réelle et sa partie imaginaire.
    
Aux nombres complexes sous forme algébrique $z=a+ib$, on peut associer le point $M$ de coordonnées $(a,b)$. Python sait comme vous, très bien le faire!
Voici un code Python qui permet d'afficher les points images de deux nombres complexes et le segment les reliant:

```py

import matplotlib.pyplot as plt

x1 = 1
y1 = 2
plt.scatter(x1, y1,color = 'red',s = 80) # Un premier point
x2 = 5
y2 = 3
plt.scatter(x2, y2,color = 'blue', s = 80) # Un second point
# Un segment reliant les points
plt.plot([x1, x2], [y1, y2], color = 'green')
plt.show() # Lancement de la fenêtre

```
!!! question "Une question?"
    De quels nombres complexes, le code précédent permet-il de trouver les points images?

!!! exo "Construire des points images"
    En utilisant le code précédent, construire les points images de $z_1=2i$,$z_2=3-i$ et $z_3=4+i$.

On rappelle que si le point $M(a,b)$ est le point **image** de $z=a+ib$, on dit aussi que $z$ est l'**affixe** du point $M$.

!!! exo "Construire encore"
    1. Dessine le point d'affixe $1$ et celui d'affixe $i$.
    2. Définis le nombre complexe $z=3-2i$ et dessine les points d'affixes $z$, $2z$, $iz$, $\bar{z}$ et $\dfrac{1}{z}$.
    3. Choisir un nombre complexe $z$ quelconque et dessinez le triangle constitués des points images de $z$, $2z$ et $(1+2i)z$. Recommencez avec plusieurs valeurs de $z$. 
    4. Soit $\omega=\dfrac{1}{2}+\dfrac{\sqrt{3}}{2}i$.Dessinez les points images de $1, \omega$, $\omega^2$ , $\omega^3$, $\omega^4$ et $\omega^5$. La racine carrée en python est ``sqrt`` que l'on doit importer en début de programme par l'instruction ``from math import sqrt``.


On va maintenant exploiter la forme trigonométrique (ou géométrique) des nombres complexes en calculant leur **module** et **un des arguments**.

!!! note "La forme trigonométrique de $z$"
    Tout nombre complexe $z$ non nul peut s'écrire sous la forme $z=\rho(\cos(\theta)+i\sin(\theta))$ où $\rho$ est le module de $z$ et $\theta$ un argument de $z$.

La forme algébrique d'un nombre complexe $z=a+ib$ définit les coordonnées **cartésiennes** $(a,b)$ de son point image alors que la forme trigonométrique $z=[\rho, \theta]$ définit les coordonnées **polaires** du point image:
<center>
![](repere_complexes.png)
</center>

On va pour cela utiliser un module dédié à cela.

!!! info "Le module ``cmath``"
    Le module ``cmath`` fournit les outils supplémentaires pour manipuler les formes trigonométriques des nombres complexes.

L'importation du module se fait en début de programme:

``import cmath``

Puis:

- l'instruction ``cmath.phase(z)`` renvoie l'argument de $\theta$ dans l'intervalle $]-\pi, +\pi]$ du nombre complexe $z$.
- l'instruction ``abs(z)`` renvoie le module de $z$.
- l'instruction ``cmath.polar(z)`` renvoie le couple $[\rho, \theta]$ , c'est-à-dire la forme trigonométrique de $z$.
- l'instruction ``cmath.rect(r, theta)`` renvoie le nombre complexe dont le module est ``r`` et l'argument $\theta$. 


!!! exo "Calculer module et argument"
    1. Pour un nombre complexe $z$, par exemple $z=1+3i$ ou $z=1+i$, calculer son module et un de ses arguments.
    2. Quel nombre complexe a pour module $2$ et argument $\dfrac{\pi}{3}$? Essaie de deviner les valeurs exactes à partir des valeurs proposées par Python
    3. À l'aide du module ``matplotlib``, place le point d'affixe $z$ de module $\sqrt{2}$ et d'argument $\dfrac{\pi}{6}$.


!!! exo "Racines de 1..."
    On considère le nombre complexe $\omega$ de module 1 et d'argument $\dfrac{2\pi}{n}$ avec $n$ un entier supérieur ou égal à 3.

    1. Dans cette question $n=5$. Dans un repère, affiche le polygone constitué  des points images des nombres complexes $1$,$\omega$, $\omega^2$, $\omega^3$
     et $\omega^4$ .
    2. Dans cette question $n$ est un **entier** saisi par l'utilisateur. Dans un repère, affiche le polygone constitué  des points images des nombres complexes $1,\omega, \omega^2, \omega^3$ jusqu'à $\omega^{n -1}$.



