

# Le cours:


<div class="centre">
<iframe 
src="./matrice/cours_Matrice_2023.pdf"
width="1000" height="500" 
frameborder="0" 
allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
</iframe>
</div>


# Les exercices:

[Fiche n°1: Calcul matriciel](./matrice/Exercice_calcul_matriciel.pdf){ .md-button target="_blank" rel="noopener" }

[Fiche n°2: Manipulation algébrique de formules ](./matrice/Exercice_Application_calcul_matriciel.pdf){ .md-button target="_blank" rel="noopener" }

# Compléments:

[Un travail sur les images et les matrices en Python ](./matrice/Matrices_Images.pdf){ .md-button target="_blank" rel="noopener" }


