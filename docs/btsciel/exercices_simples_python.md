!!! exo "Valeur des variables"

    === "Exercice"
        Recopier le code suivant et donner la valeur des variables ``d,t1, t2, t3`` et ``t4``.
        ```py
        a = 2
        b = 10
        c = 8
        c = c + 2
        d = a + b*c
        t1 = 3 > 2
        t2 = b > c
        t3 = a + b != 2*c
        t4 = d == 102
        ```
    
    === " Correction"
        ```py
        a = 2
        b = 10
        c = 8
        c = c + 2 # c vaut alors 10
        d = a + b*c # d vaut 2+10*10 soit 102
        t1 = 3 > 2 # la condition 3 > 2 est vraie donc t1 vaut True (variable booléenne)
        t2 = b > c # b vaut 10 et c vaut 10 donc b > c est faux et t2 vaut False
        t3 = a + b != 2*c # a + b vaut 12 et 2*c vaut 20 et 12 est différent de 20 donc t3 est True
        t4 = d == 102 # d vaut 102 donc la condition précédente est vraie
        ```
    
!!! exo "Structure conditionnelle"

    === "Exercice"
        1. Le code suivant contient deux instructions input qui permet de saisir une information
        (de type string pour le premier de type int pour le second) au clavier.
        ```py
        prenom = input("Donnez votre prénom")
        age = int(input("Donnez votre âge"))
        if ..........:
            phrase = "Bonjour, " + prenom + ", "+"vous êtes majeur"
        else:
            phrase = "Bonjour, " + prenom + ", " + "vous êtes mineur"
        print(phrase)
        ```

        2. En vous inspirant de cette structure algorithmique, réalisez un programme qui permet de
        saisir un prénom et une note et qui affiche « L’élève Untel a la moyenne » ou « L’élève Untel
        n’a pas la moyenne » où Untel est le prénom saisi au clavier.

    === "Correction 1"
        ```py
        prenom = input("Donnez votre prénom")
        age = int(input("Donnez votre âge"))
        if age >= 18 :
            phrase = "Bonjour, " + prenom + ", "+"vous êtes majeur"
        else:
            phrase = "Bonjour, " + prenom + ", " + "vous êtes mineur"
        print(phrase)
        ```
    === "Correction 2"
        ```py
        prenom = input("Donnez votre prénom")
        note = int(input("Donnez votre note"))
        if note >= 10 :
            phrase = "L'élève " + prenom + ", "+" a eu la moyenne"
        else:
            phrase = "L'élève " + prenom + ", "+" n'a pas eu la moyenne"
        print(phrase)
        ```

!!! exo "Histoire de degré"

    === "Exercice"
        Construire un programme Python qui permet de transformer un température saisie en degré au clavier en une température en Kelvin, affichée dans la console(pour rappel 0°C =-273 K).


    === "Correction"
        ```py
        temp_degre = float(input("Donnez votre prénom")) # le float permet de saisir des décimaux
        temp_kelvin = temp_degre - 273
        print(temp_kelvin)
        ```

!!! exo "Puissance 2 ou 3"

    === "Exercice"
        Le code suivant utilise une boucle(pour répéter...) et une f−string(pour un affichage
        dynamique) qui permet d’afficher les 20 premiers carrés.

        1. Recopier le code ci-contre et exécutez le.
        2. Modifiez le code pour afficher les 100 premiers carrés.
        3. Modifiez le code pour afficher les 100 premiers cubes.


    === "Correction"
        ```py
        for i in range(20):
            carre = i**2
            print(f"Le carré de {i} est {carre}")
        
        for i in range(100):
            carre = i**2
            print(f"Le carré de {i} est {carre}")

        for i in range(100):
            cube = i**3
            print(f"Le cube de {i} est {cube}")
        ```
    

!!! exo "Table de multiplication"

    === "Exercice"
        On cherche un code Python qui permet l’affichage des tables de multiplication. Recopier
        et compléter le code suivant qui permet d’afficher la table de multiplication de n ou n est un
        entier saisi au clavier...
        ```py
        n = int(input("Table de quel chiffre? ")
        print(f"La table de {....} est:")
        for i in range(10):
            print(f"Le produit de {...} par {...} est {...}.")
        ```


    === "Correction"
        ```py
        n = int(input("Table de quel chiffre? ")
        print(f"La table de {n} est:")
        for i in range(10):
            print(f"Le produit de {i} par {n} est {i*n}.")
        ```

!!! exo "Conversion monétaire"

    === "Exercice"
        Compéter puis recopier le programme qui affiche la table de conversion des 20
        premières sommes d’argent exprimées en euros, en dollars canadien ou dollars américains. Par
        exemple, si le cours est de 1.65 dollars canadiens cela signifie que un euro vaut 1.65 dollars.
        ```py
        s = 1
        coeff = int(input("Entrez le cours"))
        while s <= 20:
            dollar = s*coeff
            print(f"{.......} euro(s) = {..........} dollars")
            s = s + ....
        ```


    === "Correction"
        ```py
        s = 1
        coeff = int(input("Entrez le cours"))
        while s <= 20:
            dollar = s*coeff
            print(f"{s} euro(s) = {dollar} dollars")
            s = s + 1
        ```

!!! exo "Parité"

    === "Exercice"
        L’opérateur // renvoie le quotient entier de deux nombres et l’opérateur %, appelé
        modulo, le reste de la division euclidienne de deux nombres. Ainsi 10//3=3 et 10%3=1.

        1. Quelles sont les valeurs prises par m%2 où m est un nombre quelconque ?
        2. Quel renseignement sur un nombre quelconque m donne la valeur de m%2?
        3. Compléter puis recopier le code suivant :
        ```py
        m = int(input("Donner un nombre au clavier: "))
        if m%2 == 0:
            print("Le nombre proposé est .....................")
        else:
            print("Le nombre proposé est ......................")
        ```


    === "Correction"
        1. ``m%2`` vaut 0 ou 1
        2. On remarque assez vite que ``m%2`` vaut 0 si ``m`` est pair et 1 sinon.
        ```py
        m = int(input("Donner un nombre au clavier: "))
        if m%2 == 0:
            print("Le nombre proposé est pair.")
        else:
            print("Le nombre proposé est impair.")
        ```

!!! exo "Durée"

    === "Exercice"
        En utilisant le quotient entier et le modulo, écrire un programme qui transforme un
        temps donné en secondes en une durée (année,mois,semaines,jours,heures,minutes,secondes).
        On considérera qu’il y a 30 jours par mois.


    === "Correction"
        ```py
        nbre_secondes = int(input("Donner le nombre de secondes: "))
        #-----------CONSTANTES DE TEMPS  ------------#
        minute = 60
        heure = 60*minute #nombre de secondes dans 1 heure
        jour = 24*heure #nombre de secondes dans 1 jour
        mois = 30*jour #nombre de secondes dans 1 mois
        annee = 12*mois #nombre de secondes dans 1 annee
        #--------------------------------------------#

        #nombre d'années entières dans nbre_secondes
        AN = nbre_secondes//annee
        reste_AN = nbre_secondes%annee
        #nombre de mois entiers dans ce qui reste
        MOIS = reste_AN//mois
        reste_MOIS = reste_AN%mois
        #nombre de jours entiers dans ce qui reste
        JOUR = reste_MOIS//jour
        reste_JOUR = reste_MOIS%jour
        #nombre d'heures qui reste
        HEURE = reste_JOUR//heure
        reste_HEURE= reste_JOUR%heure
        #nombre de minutes dans ce qui reste
        MINUTE = reste_HEURE//minute
        reste_MINUTE = reste_HEURE%minute

        print(f"Avec {nbre_secondes} secondes, on peut faite {AN} années {MOIS} mois {JOUR} jours {HEURE} heures {MINUTE} minutes et reste {reste_MINUTE} secondes")

        ```

!!! exo "Hasard"

    === "Exercice"
        On a souvent besoin de créer des nombres aléatoires et on utilise l’instruction
        randint du module random pour générer un nombre entier au hasard.
        Par exemple, l’instruction ``a = randint (0, 100)`` affecte à la variable a une valeur entière entre 0 et
        100 compris.
        
        1. Créer une variable ``de`` qui simule le lancer d’un dé à 6 faces.
        2. Créer une variable ``note`` qui simule une note entre 0 et 20.

    === "Correction"
        ```py
        from random import randint

        de = randint(1, 6)
        note = randint(0, 20)
        ```
!!! exo "Somme"

    === "Exercice"
        Écrire un programme Python qui permet à l’aide d’une boucle de calculer les
        sommes $S = 1 + 2 + 3 + 4 + ... + 99 + 100$ et $S = 1 + 3 + 5 + 7 + ... + 97 + 99$.

    === "Correction 1 "
        ```py
        s = 0
        u = 1
        for i in range(100):
            s = s + u
            u = u + 1
        ```
    === "Correction 2 "
        ```py
        s = 0
        u = 1
        for i in range(50):
            s = s + u
            u = u + 2
        ```