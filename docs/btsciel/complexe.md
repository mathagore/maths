Un rappel de cours et une multitude d'applications... avec un regard particulier sur les fonctions de transfert des filtres analogiques.


# Le cours:

<div class="centre">
<iframe 
src="./complexes/crs_nombres_complexes_2023.pdf"
width="1000" height="500" 
frameborder="0" 
allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
</iframe>
</div>


# Compléments:

[Étude des fonctions de transfert ](./complexes/Exercice_complexes_impedances.pdf){ .md-button target="_blank" rel="noopener" }

