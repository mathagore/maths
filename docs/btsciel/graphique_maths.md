# Faire des graphiques avec Python

Pourquoi faire des graphiques? C'est une habitude pour visualiser une quantité de données:

- en physique, on réalise une expérience et on obtient des données dans un tableau que l'on exploite pour faire un graphique. 
- en maths, pour déterminer la courbe représentative d'une fonction $f$.
- en informatique, pour observer par exemple la répartition des niveaux de gris dans une image en noir et blanc.
- ...

Dans tous les cas, on utilisera la bibliothèque ```matplotlib``` qui offre tout un tas de possibilités pour faire un graphique.

!!! tip "La méthode générale!"
    Pour faire un joli graphique, il faut en général, une liste de valeurs pour les abscisses et une liste de **même taille** pour les ordonnées!

Tout le reste ne sert qu'à illustrer le graphique avec des graduations, des légendes, ...

## À partir d'un tableau de nombres:

Dès la classe de sixième, vous avez pris l'ahabitude de construire dans un repère cartésien du plan, des points dont les coordonnées sont données par un tableau:


|x|0|1|3|4|6|
|-|-|-|-|-|-|
|y|0|4|1|2|4|


et le code python qui permet de représenter soit le nuage de point soit la courbe passant par ces points:

!!! example "Tracés simples"

    === "Nuage de points"

        ```py
        import matplotlib.pyplot as plt
        X = [0, 1, 3, 4, 6]
        Y = [0, 4, 1, 2, 4]
        plt.scatter(X, Y)#scatter pour les points seulement
        plt.show() # affiche la figure à l'écran
        ```
    
    === "Interpolation par une courbe"

        ```py
        import matplotlib.pyplot as plt
        X = [0, 1, 3, 4, 6]
        Y = [0, 4, 1, 2, 4]
        plt.plot(X, Y)#plot pour courbe
        plt.show() # affiche la figure à l'écran
        ```
Vous devriez obtenir ceci:

<center>
![](<../pyth/img/graphique1.png>){width=40%}
![](<../pyth/img/graphique2.png>){width=40%}
</center>
!!! exercice "À vous de jouer"
    1. Réaliser le nuage de points obtenus par les deux listes ``(X, Y)`` où ``X = [i for i in range(11)]`` et ``Y = [12, 0, 15, 7, 12, 2, 18, 17, 10, 9, 4] ``. 
    2. Allez à cette adresse [ici](https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.scatter.html) pour vous documenter sur la façon de changer la couleur et la forme des points.
    3. Ajouter le nuage de points ``(X, Z)`` où ``Z = [randint(0, 20) for i in range(11)]``. On n'oubliera pas d'importer le module nécessaire ``from random import randint``. Les deux nuages de points auront des couleurs et des formes différentes.

## À partir d'une fonction mathématique:

Souvent, il faut tracer la courbe représentative d'une fonction mathématique.

!!! example "La courbe représentative d'une fonction mathématique"
    Rien de plus simple! Il faut:

    - une fonction $f$ définie sur un intervalle $I$
    - une liste en abscisse
    - une liste en ordonnée

Voici le code à adapter selon les besoins...

??? exemple "Pour tracer une courbe représentative..."
    ```py 
    import numpy as np
    import math
    import matplotlib.pyplot as plt
    #des objets pour tracer...
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    #valeurs max et min des axes
    x_min = 0
    x_max = 0.5
    y_min = 0
    y_max = 2
    #la fonction 
    def f(x):
        return 1/np.sqrt(1.25 - np.cos(2*np.pi*x))
    #la liste en abscisses   
    X = np.arange(start = x_min, stop = x_max , step = 0.01)
    #delimitation des axes
    plt.xlim(x_min,x_max)
    plt.ylim(y_min,y_max)
    #trace de la courbe
    plt.plot(X, f(X),'r')
    plt.xlabel(r"x")
    plt.ylabel(r"f(x)")
    plt.title(r"Courbe")
    plt.show()
    ```

!!! exercice "Recopier et utiliser"
    Copier et exécuter le code Python précédent. De quelle fonction a t-on tracé la courbe représentative?

        
Autre exemple, voici le code ```python``` pour construire la courbe représentative des fonctions $\cos$ et $\sin$ sur l'intervalle $I=[0, 2\pi]$.

??? exemple "Fonctions trigonométriques"
    ```py
    ####### import bibliothèques ###############
    import numpy as np
    import math
    import matplotlib.pyplot as plt
    ####### une figure et des axes ############
    fig, ax = plt.subplots()
    ###### valeurs extrêmes en X et Y #########
    x_min = 0
    x_max = 2*np.pi
    y_min = -1 #valeur minimale du cos et sin
    y_max = 1 #valeur maximale du cos et sin
    ##### fonction cos et sin #################
    def lesinus(x):
        return np.sin(x)
    def lecosinus(x):
        return np.cos(x)
    ##### génération de la liste des abscisses
    X = np.arange(start = x_min, stop = x_max, step = 0.25)
    #### limite horizontale du graphique ######
    plt.xlim(x_min, x_max)
    #### limite verticale du graphique ########
    plt.ylim(y_min, y_max)
    ###### pour les grilles ###################
    grid_x_ticks = np.arange(x_min, x_max, 0.5)
    grid_y_ticks = np.arange(y_min, y_max, 0.25)
    ###### pour les graduations
    ax.set_xticks(grid_x_ticks, minor= False)
    ax.set_yticks(grid_y_ticks , minor= False)
    #### pour l'affichage de la grille ########
    ax.grid(which='both')
    ### construction des deux courbes
    # X est la première liste et lesinus(X) est la deuxième
    plt.plot(X, lesinus(X), color ='red', label ='cos')
    plt.plot(X, lecosinus(X), color = 'green', label ='sin')
    ##### pour la légende #####################
    plt.xlabel(r"$x$ ")#syntaxe latex
    plt.ylabel(r" $\sin(x)$ et $\cos(x)$")#syntaxe latex
    plt.legend()
    plt.title(r"Fonctions trigonométriques ")
    plt.show()
    ```


!!! exercice "Tracer des sinusoïdes"
    À l'aide de l'exemple précédent, construire sur des différents graphiques, la courbe représentative:

    1. de la fonction $f(t)=10\sin(2t)$ pour $t$ dans l'intervalle $[0; 2\pi]$.
    2. des fonctions $g(t) = 5\cos(4t)$ et $h(t) = 8 \sin(4t)$ sur l'intervalle $[0; \pi]$
    3. de la fonction $S(t)= g(t) + h(t)$ sur l'intervalle $[0; \pi]$.

Le sinus cardinal est une fonction mathématique très utilisée en traitement du signal.

!!! exercice "Le sinus cardinal"

    1. Compléter le code suivant afin de construire la courbe représentative du sinus cardinal et de son carre.
    2. Sur quel intervalle sont représentées ces courbes? Doublez cette intervalle.

```python
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats

x = np.linspace(-4 * np.pi, + 4 * np.pi, 200)
y = np.sinc(x / np.pi)

fig, ax = plt.subplots(1, 1)
ax.plot(x, y, label = '.....')       # Tracé y(x)
ax.plot(x, y**2, label = '......')  # Tracé y²(x)
ax.set(title = 'sinc et sinc²',    # Titre et intitulé des axes
       xlabel = '$x$', ylabel= '$y $')
ax.legend()
plt.show()
```

Un truc sympa pour montrer quelques possibilités du module ``matplotlib``...

Les trèfles de Habenicht sont des courbes «ornementales» en représentation polaire, d'équation
$r_n(\theta) = 1 + \cos n\theta + \sin^2 n\theta, \qquad n\in \mathbb{N}^*$
    
La fonction suivante permet de  calculer $r_n$ en fonction de $n$ et $\theta$:
```python
def habenicht(n, theta):
    return 1 + np.cos(n * theta) + np.sin(n * theta) ** 2
```

On génère alors un ensemble de valeurs par l'appel successif des fonctions sur différentes entrées:

```python
theta = np.linspace(0, 2 * np.pi, 200) #generation de 200 valeurs de theta

r3 = habenicht(3, theta) 
r5 = habenicht(5, theta)
r7 = habenicht(7, theta)

```

Puis le graphique qui nous donne les **courbes polaires**:

```python
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1, projection='polar')
ax.plot(theta, r3, label='n=3')
ax.plot(theta, r5, label='n=5')
ax.plot(theta, r7, label='n=7')
fig.legend()
plt.show()
```

!!! exercice "Trèfles de Habenicht" 
    1. Rassembler les bouts de codes précédents pour construire les courbes polaires correspondantes.
    2. Les trèfles ont été tracés pour $n=3,5,7$. Choissisez de nouvelles valeurs de $n$.
    3. Pour les meilleurs d'entre vous, construire la courbe dont l'équation polaire est $r(\theta)=e^{\cos(\theta)}-2\cos(4\theta)+\sin^5(\dfrac{\theta}{12})$

## Nombres complexes et représentation

Python et numpy gèrent nativement les complexes (le nombre imaginaire pur est noté $j$). Pour un complexe $z = a + bj = \rho\,e^{j\theta}$, on a :

- `z = a + b * 1j` ou `z = rho * np.exp(1j * theta)`
- parties réelle et imaginaire: `a = np.real(z)`, `b = np.imag(z)`, 
- module et argument: `rho = np.abs(z)`, `theta = np.angle(z)` (en radians).

!!! exercice "Utiliser les fonctionnalités de numpy"
    
    1. Soit le nombre complexe  $z_1 = 1 + 2j$ qui en python s'écrit `z_1 = 1 + 2 * 1j`. Faites afficher dans la console, le module et un argument de $z_1$.
    2. Soit le nombre complexe $z_2 =10\times e^{1.05j}$ qui en python s'écrit `z_2 = 10 * np.exp(1j * 1.05)`. Faites afficher dans la console, la partie réelle et imaginaire de $z_2$.

La fonction de transfert $H$ d'un filtre est définie comme le rapport entre le signal (complexe) de sortie $S$ et le signal (complexe) d'entrée $E$: $H = S/E$. 

$H$ est une grandeur complexe fonction de la pulsation $\omega$, et dont le module donne le facteur d'atténuation/amplification (*gain*) et l'argument le déphasage entre les signaux de sortie et d'entrée (*phase*).

!!! exercice "Passe bas"
    La fonction de transfert d'un filtre est donnée par la fonction $H(x)=\dfrac{1}{1+jx}$.
    Utiliser les codes python ci-dessous pour faire apparaître l'évolution du **module** de $H$ en fonction de $x$ puis celui de son **argument**.

La fonction $H$ en python.

```python
 def H(x):
    """ Fonction de transfert complexe """
    return 1/(1 + 1j*x)
```

On définit la fonction `gain_dB(H)` retournant le gain en dB de la fonction de transfert complexe `H`: $G_{dB} = 20\log_{10} |H|$.  

```python
def gain_dB(H):
    "Gain en dB."
    return 20 * np.log10(np.abs(H))
```

Le gain est donné dans un repère semi-logarithmique.

```python
x = np.logspace(-1, 1, 100)

fig, (ax1, ax2) = plt.subplots(2, 1)

# Gain [dB]
ax1.plot(x, gain_dB(H(x)))

# Phase [rad]
ax2.plot(x, np.angle(H(x)))

ax1.set(ylabel = 'Gain [dB]', xscale = 'log', title = 'Filtre passe-haut du 2e ordre')
ax2.set(xlabel = 'x', xscale = 'log', ylabel = 'Phase [rad]')

ax1.grid(which = 'both')
ax2.grid(which = 'both')

plt.show()
```
!!! exercice "Filtre passe haut"
    Reprenez le travail précédent avec la fonction $H(x)=\dfrac{x}{1+jx}$

Nous allons maintenant étudier le filtre passe-haut du 2e ordre dont la fonction de transfert complexe est:$H_Q(x) = \frac{-x^2}{1 - x^2 + j x/Q}$ où $x = \omega/\omega_0$ est la pulsation réduite, et $Q>0$ le facteur de qualité.

Nous allons pour précédemment, tracer son *diagramme de Bode*, constitué de deux graphiques représentant le comportement fréquentiel (en échelle logarithmique) de son gain (exprimé en dB) et de sa phase. 

```python

def H_PH2(x, Q = 1):
    """
    Fonction de transfert complexe d'un filtre passe-haut du 2e ordre.
    
    x: pulsation réduite
    Q: facteur de qualité
    """
    
    return -x**2 / (1 - x**2 + 1j * x / Q)

def gain_dB(H):
    "Gain en dB."
    
    return 20 * np.log10(np.abs(H))

x = np.logspace(-1, 1, 100)

fig, (ax1, ax2) = plt.subplots(2, 1)

# Gain [dB]
ax1.plot(x, gain_dB(H_PH2(x, Q = 0.2)), label='Q=0.2')
ax1.plot(x, gain_dB(H_PH2(x, Q = 1)), label='Q=1')
ax1.plot(x, gain_dB(H_PH2(x, Q = 5)), label='Q=5')

# Phase [rad]
ax2.plot(x, np.angle(H_PH2(x, Q = 0.2)))
ax2.plot(x, np.angle(H_PH2(x, Q = 1)))
ax2.plot(x, np.angle(H_PH2(x, Q = 5)))

ax1.set(ylabel = 'Gain [dB]', xscale = 'log', title = 'Filtre passe-haut du 2e ordre')
ax2.set(xlabel = 'Pulsation réduite x', xscale = 'log', ylabel = 'Phase [rad]')

ax1.grid(which = 'both')
ax2.grid(which = 'both')
```
!!! exercice "Un de plus"
    Ajouter les instructions qui permettent de visualiser le comportement du filtre pour $Q = 0.1$ et $Q = 10$.
