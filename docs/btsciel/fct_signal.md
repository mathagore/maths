

# Le cours:


<div class="centre">
<iframe 
src="./fct_signal/crs_fonction_modelisation.pdf"
width="1000" height="500" 
frameborder="0" 
allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
</iframe>
</div>


# Les exercices:

[Fiche n°1: Manipuler des fonctions de transfert](./fct_signal/exercices_fonctions_transfert.pdf){ .md-button target="_blank" rel="noopener" }

[Fiche n°2: Manipulation algébrique de formules ](./fct_signal/exercice_manip_algebrique.pdf){ .md-button target="_blank" rel="noopener" }

# Compléments:

[Comportements asymptotiques des fonctions de transfert ](./fct_signal/exercice_comportement_fonction_transfert.pdf){ .md-button target="_blank" rel="noopener" }


