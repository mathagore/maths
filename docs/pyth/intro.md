---
hide:
  - toc
---

# GRENIER 

Pour ranger le bordel...

!! question "Une légère différence!"

    === "Question" 
        Connaissez-vous la différence entre ```=``` et ```==```?

    === "Réponse"
        La première instruction permet d'affecter une valeur à une variable , la deuxième permet de comparer la valeur de deux variables.


Donc par exemple,

- l'instruction ``retard = 2`` affecte à la variable ``retard`` la valeur ``2``
- l'instruction ``retard == 2`` compare la valeur de la variable ``retard`` à 2. Cette opération (oui, c'est une opération...) donne ``True`` si la condition est vraie et ``False`` sinon...


!!! example "Repérer un bloc d'instructions"

    === "En `python`"
        ```py
        i = 0
        while i < 5:
            print(f"La variable i vaut {i})
            i = i + 1
        ```
    === "En `C`"
        ``` C
        int i = 0;

        while (i < 5) {
            printf("La variable i vaut %d\n", i);
            i++;
        }
        ```

    === "En `Javascript`"
        ``` javascript
        let i = 0
        while (i < 5){
            console.log(i);
            i++;
        }
        ```


!!! exo "Corrigez les erreurs"
    Le code ci-dessous contient quatre à cinq erreurs: saurez-vous les trouver?
    ```python
    pnom = imput("Bonjour, quel est votre prénom?")
    if pnom = "Manu"
    print("tu est le président ?")
    else:
    print("Je ne connais pas....!")
    ```



??? tip "Que peut-on faire avec des chaînes de caractères?"

    Les opérateurs sur ces objets sont des opérateurs de **lecture** et de **concaténation**. Mais il n'existe pas d'opérateur d'écriture qui modifierait la chaîne de caractères.

Cela signifie qu'une fois déclarée, une variable de type `string` ne peut plus être **modifiée**.

``` py
nom = "Alfred" 
club = "Madrid"
age = "30"
but = 26
print(nom[0]) #affiche le premier caractère de la variable nom
print(club[2]) #affiche le caractère situé au rang 2 de la variable club
p = nom + " a marqué " + str(but) + " buts avec le club de " + club + " à l âge de " + age + "ans."
```
L'instruction `print` affiche la valeur de la variable. La variable `phrase_du_jour` est obtenue par la **concaténation** de plusieurs chaînes de caractètres. L'opérateur de concaténation est aussi le $+$ mais ne s'applique qu'aux chaînes de caractères! La variable `but` est **transtypée** pour passer d'un `int` à un `string`!
On obtient alors:

``` py
>>> print(phrase_du_jour)
Alfred a marqué 26 buts avec le club de Madrid à l âge de 30 ans.
```

Attention en revanche, on ne peut pas concaténer une chaîne de caractères et un entier:

``` py
>>> print("buts marqués" + but)
TypeError: unsupported operand type(s) for +: 'int' and 'str'
```

Rappelons aussi l'usage des ```f-string``` qui permet de formater des chaînes de caractères avec la valeur de certaines variables...



!!! warning "Tableau ou liste"

    Le langage Python appelle les tableaux **liste** et crée une confusion dans ce qu'est réellement un tableau en informatique. 


Explorons le code (stupide...) suivant:
```py 
a = 3
def f(x):#stupide car le nom de la fonction est trop générique et qu'il y a des confusions dans le nom des variables...
    a = x**2
    return a
f(5)
print(a)
```

D'après vous que vaut la variable ```a``` après l'exécution de ce code?
??? tip " À savoir pour comprendre"
    Il y a deux variables ```a```: 

      - la première est déclarée en **dehors**  du corps de la fonction: elle est donc globale et vaut 3
      - la deuxième est déclarée **dans** la fonction: elle est locale et vaut le carré de la valeur prise par le paramètre ```x``` lors de l'appel de la fonction.
    L'affectation locale de ```a``` n'a pas d'effet sur la valeur globale ```a```. Après son exécution, ce code donne ```a=3```....

On dit que les variables n'ont pas la même portée... Dernier exemple rigolo avant de passer à autre chose...


!!! question " Anticipez ce que va faire ce programme."
    === "Code"

        ``` py
        def multiplie_par_2(nombre):
            i = 2
            return nombre * i
        print(multiplie_par_2(8))
        print(i)
        ```
    === "Réponse"
    
        La variable ```i``` est locale à la fonction et le programme principale n'a qu'une vision globale des variables et ne peut pas visualiser ```i``` dans le corps de la fonction : il ne peut donc pas savoir ce que vaut ```i``` et encore moins l'afficher. Python va donc lever une exception!

    

!!! example "Extrait d'un cours de NSI"
    Supposons coder un message à la manière des soldats de Jules César (ancien Empereur romain assez connu). Le principe est assez simple: un décale de trois rangs dans l'alphabet latin les lettres. ```A``` devient alors ```D```, ```B``` devient ```C``` ,... puis ```X``` devient ```A```, ```Y``` devient ```B``` et enfin ```Z``` pour ```C```!
    Les fonctions ```python ``` suivantes permettent ce codage:
    ```py
    def carac_en_nombre(carac):
          """ donne le code numérique du caractère carac
          de la table ASCII
          a:97,b:98,...
          """
          return ord(carac)

    def nombre_en_carac(nbre):
        """ donne le caractère correspondant au nombre nbre
          de la table ASCII
          97:a,98:b,...
          """
          return chr(nbre)

    def decalage(nbre, cle):
        """ ajoute la valeur de cle au nombre nbr
        on travaille modulo 26 pour que les sommes restent dans
        l'intervalle [0; 25]
        les translations de 97 permettent de travailler dans l'intervalle [0,25]
        """
        return (nbre + cle - 97)%26 + 97
    
    def codage_cesar(texte_origine):
        texte_final = ""#chaine de caractere vide
        for elt in texte_origine: #on parcourt le message elt par elt
            texte_final = texte_final + nombre_en_carac(decalage(carac_en_nombre(elt), 3))
        return texte_final
    message = "vous ne trouverez jamais la solution"
    MESSAGE = codage_cesar(message)
    ```