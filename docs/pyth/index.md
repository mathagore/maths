

```Python``` est un langage de haut niveau, proche du langage humain. Sans être un expert, on peut facilement comprendre et analyser un **programme** python .

Il faut distinguer les niveaux de compétences:

- **utiliser** du code pour automatiser une tâche, faire des calculs,...;
- **modifier du code** existant pour l'adapter aux besoins des exercices demandés;
- **construire de code** en partant d'une feuille blanche.

C'est en général, les deux premières compétences qui seront travaillées en mathématiques.


!!! info "Pour faire du python..."

    il faut faire du python! 

    
En pratiquant, vous allez améliorer vas capacités. Donc au travail!

Vous trouverez un parcours qui vous permettra de découvrir le langage Python selon le principe suivant:

- des contenus théoriques :fontawesome-solid-user-graduate:
- des exercices corrigés :fontawesome-solid-marker:
- des exercices non corrigés :fontawesome-solid-wrench:

Les contenus amènent de nouvelles connaissances, les exercices corrigés les utilisent et les exercices non corrigées vous permettent de vous exercez.

