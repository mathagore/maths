# Générer du hasard

Dans de nombreux cas, nous aurons besoin de simuler le hasard: plus précisément, certaines variables devront être affectées avec une valeur aléatoire. On distingue en général, les valeurs **entières** des valeurs **décimales**.

Pour cela, il faut **importer** la bibliothèque qui génére les nombres aléatoires:

```py
from random import randint
```

Cette  instruction n'importe **que** la méthode ```randint``` : la bibliothèque ```random``` en contient d'autres que nous n'utiliserons pas ici!

On considère le programme suivant:

```py
n = randint(0, 100)
mon_choix = int(input("proposez un nombre"))
if n == mon_choix:
    print("C'est gagné")
else:
    if n > mon_choix
        print("C'est plus")
    else:
        print("C'est moins")
```

!!! note  "À faire : Utiliser le hasard"
    1. Que fait l'instruction ```randint(0, 100)```?
    2. Quelle est la différence entre les instructions ```n = mon_choix``` et ```n == mon_choix```?
    3. À quoi sert le ```int``` devant le ```input```?
    4. Commenter chaque instruction de ce code comme dans l'exemple test.

