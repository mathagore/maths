
<link rel ="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.7.2/css/all.min.css">

<h2 class = "python"><span class="fa-3x">
  <i class="fa-solid fa-user-graduate fa-flip fa-sm" style="--fa-animation-duration: 5s;"></i>
  
</span> Répéter des instructions!</h2>


En programmation, on est souvent amener à répéter une ou plusieurs instructions. On utilise alors des **boucles** pour y parvenir.

!!! example "Lancer de dé"
    Imaginons un élève qui veut simuler le lancer consécutifs de 5 dés. Cela pourrait donner en Python:
    ```py
    from random import randint
    print("Le lancer du dé 1 est " , randint(1, 6))
    print("Le lancer du dé 2 est " , randint(1, 6))
    print("Le lancer du dé 3 est " , randint(1, 6))
    print("Le lancer du dé 4 est " , randint(1, 6))
    print("Le lancer du dé 5 est " , randint(1, 6))
    ```
    Sans doute pouvons-nous faire mieux. En effet, l'utilisation d'une boucle et d'une ``f-string`` donne:
    ```py
    from random import randint
    for i in range(5):
        print(f"Le lancer du dé {i + 1} est {randint(1, 6)}")
    ```
    La variable de compteur ``i`` allant de 0 à 4, je lui ai ajouté 1 pour un affichage plus cohérent.

!!! info "Répétition de code"
    À chaque fois que vous répétez du code, on peut certainement boucler pour condenser ces instructions.

Il existe essentiellement deux façons de **boucler** que nous allons voir ici.

## La boucle ``for i in range(N):``

Elle permet de réaliser la répétition d'une ou plusieurs instructions
selon le principe suivant:

À chaque fois que la variable entière ```i``` prend une valeur entre ```0``` et
```N-1```, le bloc d'instruction est exécuté.

```py
for i in range(5):#i prend les valeurs entieres de 0 à 4, soit 5 valeurs
    print("J'ai tout compris!")#affiché 5 fois
```

Les instructions du bloc peuvent évoluer selon les valeurs de la
variable ```i``` comme le montre l'exemple suivant:

```py
for i in range(10):
    print(2*i)#l'affichage dépend de la valeur de i
```
    

!!! example "Rappel"
    L'**indentation** est importante: elle permet d'indiquer quelles instructions sont répétées. 

Si l'indentation n'est pas respectée, un message d'erreur apparaît dans la console.

!!! exo "Choix de l'indentation"
    === "Exercice"
        Quelle est la différence entre les deux codes suivants pourtant voisins?
        ```py
        #-----code1.py------#
        u = 1
        for i in range(50):
            u = 2 * u
            print(u)
        #-----code2.py------#
        u = 1
        for i in range(50):
            u = 2 * u
        print(u)
        ```
    === "Correction"
        Dans le premier code, il y a **deux instructions** réalisées à chaque boucle. En particulier, il y a 50 affichages dans la console.

        Dans le second, il n'y a qu'une instruction dans la boucle qui fait évoluer la valeur de ``u``. À la fin des boucles, la valeur de ``u`` est alors affichée.


!!! info "Le range"
    Par défaut, l'instruction ```for i in range(N):``` permet de répéter ``N`` fois les instructions contenus dans la boucle.

La variable ``i`` itère de l'entier ```0``` à l'entier ```N-1``` (il y a bien ```N``` entiers) avec un pas de 1. Ces paramètres peuvent être redéfinis. À chaque fois que ```i``` prend l'une de ces valeurs, on effectue les instructions dans la boucle.

De façon générale, la boucle ```for``` est souvent utilisée pour parcourir
tout **objet itérable** comme une **chaîne de caractères** ou une **liste**:

```py
phrase = "J'aime les papates crues"
for elt in phrase:
    print(elt, end = ',')
```
Dans la boucle précédente, l'itérateur ```elt``` prend comme valeur successivement les caractères qui constituent la chaîne de caractères, soit ```J```, puis ```'```, puis ```a```, puis ```i```, puis ```m```,... 

!!! exo "Décrire une évolution"
    === "Exercice classique"
        1. Décrire dans un tableau l'évolution de la valeur des variables mises en jeu dans le programme ci-après.
        2. Qu'affiche ce programme à la fin de son exécution?

        ```py
        S = 0
        for i in range(10):
            S = S + i
        print(S)   
        ```
    === "Correction"
        La variable ``S`` étant initialisée à ``0`` , à chaque tour de boucles on lui ajoute la valeur de ``i``. Donc on lui ajoute d'abord ``0``, puis ``1``, puis ``2``,... , et enfin ``9``. Donc ``S`` vaut ``0``, puis ``1`` puis ``3``, puis ``6``, puis ``10``,...

        À la fin ``S`` vaut donc ``S = 0 + 1 + 2 + 3 + 4 + ... + 8 + 9`` soit ``45``.


## La boucle ``while``

Elle permet de répéter un bloc d'instructions **tant que** une condition
est vraie. Elle nécessite pour être utilisée correctement:

-   la condition doit être **initialisée** avant la boucle;

-   il doit y avoir **un test** dans la condition;

-   l'**état de la condition** doit être modifié dans la boucle.

L'exemple suivant permet de déterminer combien de fois il faut
multiplier 1 par 2 pour dépasser 100:

```py
compteur = 0
u = 1
while u < 100:
    u = 2*u
    compteur = compteur + 1
print(compteur) 
```

Quelques explications:

-   les variables ```compteur``` et ```u``` sont initialisées au début;

-   la condition ```u<100``` est vraie au départ; elle a pu être **évaluée**
    car ```u``` a été initialisée. On **rentre** alors dans la boucle.

-   l'instruction ```u=u*2``` change la valeur de ```u``` de telle façon que la
    condition ```u<100``` va devenir **fausse** au bout d'un moment.

-   dès qu'elle est fausse, on sort de la boucle et on effectue la
    dernière instruction d'affichage.


!!! question "Que dire de tels codes?"
    
    === "Questions"

        ```py
        #------------code1------------#
        i = 0
        while i < 4:
            fd(50)
            lt(90)
        #------------code2------------#
        i = 10
        while i < 5:
            print(i)
            i = i + 1
        ```

    === "Réponses"
        Le premier boucle à l'infini et pour le deuxième, on ne rentre pas dans la boucle car la condition ```i < 5``` n'est pas vraie dès le départ.

Néanmoins dans certaines situations, on pourra utiliser une boucle
infinie; par exemple un serveur qui écoute sur un port et attend qu'un
client se manifeste.

On utilise alors une condition qui est toujours vraie comme **True**,
variable booléenne (au tout autre valeur non nulle). L'instruction
**break** permettra alors d'arrêter la boucle dès que survient un
événement défini par l'utilisateur.
```py
from random import randint
somme = 0
while True:
    n = randint(0,100)
    if n == 50:
        break
    somme = somme + n
print(f"Vous avez gagné {somme} euros")
```


<h2 class = "python"><span class="fa-3x">
  <i class="fa-solid fa-marker fa-flip fa-xs" style="--fa-animation-duration: 5s;" ></i>
  
</span> Exercices corrigés</h2>

!!! exo "Évolution"
    
    === "Exercice"
        Qu'affiche ce programme?
        ```py
        a = 2
        for i in range(20):
            a = a + 1
        print(a)
        ```
    === "Correction"
        ``a`` vaut 2 puis à chaque tout de boucle, on lui ajoute ``1``. Donc ``a`` vaut $2 + 20\times 1= 22$ et le programme affiche donc ``22``.
    
!!! exo "Carré magique"
    === "Exercice"
        Qu'affiche ces instructions dans la console?
        ```py
        for i in range(10):
            print(i**2)
        ```
    === "Correction"
        À chaque tour de boucles, le code affiche le carré de la valeur de ``i`` qui va de ``0`` à ``9`` de ``1`` en ``1``. Donc le programme affiche ``0``,``1``,``4``,``9``,...,``81``.


<h2 class = "python"><span class="fa-3x">
  <i class="fa-solid fa-wrench fa-flip fa-xs" style="--fa-animation-duration: 5s;" ></i>
  
</span> Exercices</h2>

!!! exo "Table de multiplication"
    1. Écrire un programme qui affiche les 22 premiers multiples de ``7``.
    2. Écrire un programme qui affiche les 15 premiers multiples de ``5``.
    3. Écrire un programme qui:
        - permet de saisir un entier ``N`` au clavier
        - affiche les 10 premiers multiples de ``N``.

!!! exo "Suite arithmétique"
    Écrivez un programme qui:

    - affiche les 20 premiers termes de la suite arithmétique de premier terme 2 et de raison 5
    - affiche le 2890ième terme seulement de la suite arithmétique de premier terme 10 et de raison 12.23
    - affiche la somme des ``N`` premiers termes de la suite arithmétique de premier terme 1 et de raison 1, ``N`` étant un entier 
    saisi par l'utilisateur.

!!! exo "Suite géométrique"
    Écrivez un programme qui affiche une suite de 10 nombres dont chaque terme est égal au triple du terme précédent. 

!!! exo "Terme consécutif d'une liste"
    Écrire un programme qui compte les entiers consécutifs d'une liste.
    Par exemple, la liste ``[1, 2, 3, 4, 5]`` donnerait ``4`` , la liste ``[10, 21, 22, 2, 5, 7, 8]`` donnerait ``2``, ...

!!! exo "Descente dichotomique"
    Tout le monde connaît le jeu du juste prix qui consiste à trouver le bon prix par le jeu des questions réponses, ```c'est plus!```, ```c'est moins!```.

    1. Créer les instructions qui permettent:
        - de choisir un entier ```alea``` entre 1 et ```n```.
        - de proposer un entier ```choix```.
        - d'afficher la comparaison de ces deux entiers
        - et de recommencer jusqu'à la bonne réponse.
    2. Créer les mêmes instructions mais cette fois-ci, l'ordinateur joue tout seul (c'est lui qui propose un nouveau nombre à chaque fois !)
    3. Créer une nouvelle instruction qui compte le nombre de questions posées avant de trouver la bonne réponse.
    4. Créer un graphique qui prend en abscisse le nombre ```n```(choix maxi de ```alea```) et en ordonnée, le nombre maximum de questions posées pour retrouver l'entier ```alea```(comme précédemment...)


!!! abstract "Ce qu'il faut retenir"
    - Il faut savoir utiliser une boucle simple du type ``for i in range(10):`` 
    - Il faut identifier les instructions qui sont répétées dans une boucle.
    - Dès qu'on répète du code, on peut certainement le remplacer par une boucle.



