<link rel ="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.7.2/css/all.min.css">

<h2 class = "python"><span class="fa-3x">
  <i class="fa-solid fa-user-graduate fa-flip fa-sm" style="--fa-animation-duration: 5s;"></i>
  
</span> Des types pas très clairs!</h2>


Rappelons-le encore une fois, les variables ont des **types**. À leur création, on leur affecte une **valeur** dont le type peut-être à la louche:

- un nombre: un ```int``` ou un ```float``` . Pour faire simple, soit on affecte un nombre entier soit un nombre décimal( il existe d'autres types dont nous ne parlerons pas ici)
- une chaîne de caractères ou **string** pour faire court. C'est un texte contenu dans une chaîne qui se déclare entre deux quotes (".......").

Ces deux types sont essentiels.


Mais ils ne suffisent pas en général pour répondre à tous nos besoins. En particulier, le besoin de conserver dans une seule variable, plusieurs valeurs obtenues par exemple par simulation, est indispensable. 

Nous allons définir une variable dont le type est ``list``.

## Un tableau , une liste.

!!! info "De quoi s'agit-il?"

    Une liste est une séquence ordonnée de valeurs dont l'**accès aux éléments se fait par leur indice**.

Voici par exemple un tableau classique de nombres:

<center>
![](img/tabliste.png){ width=30% }
</center>

Appelons ```T``` ce tableau, cette liste. Alors:

- la valeur situé au rang 0 de ```T``` est 10
- la valeur situé au rang 1 de ```T``` est 2
- la valeur situé au rang 2 de ```T``` est 8
- la valeur situé au rang 6 de ```T``` n'existe pas !

!!! info "Comment déclarer un tableau, une liste en Python?"

    Une liste en python se déclare par des crochets et les éléments sont séparés par des virgules ``,``.

Par exemple, pour notre tableau précédent:

``` py

T = [10, 2, 8, 205, -1, 0]

```


Comme les différents types de variables évoquées en préambule, il existe des méthodes propres aux listes que nous allons présenter ici.

!!! info "Connaître le nombre d'éléments d'une liste(opération de lecture)"

    Pour une liste ```L``` connue, l'instruction ```len(L)``` retourne le nombre d'éléments de la liste ```L```.

``` py

T = [10, 2, 8, 205, -1, 0]
print(len(T))#doit afficher 6 dans la console...
```

Les listes sont parfois très grandes et on ne peut pas compter _manuellement_ le nombre de ces éléments...


## Accès aux éléments d'une liste

!!! info "Accès à un élément de la liste(opération de lecture)"

    On peut accèder aux valeurs des éléments de la liste par la connaissance de son rang, son indice.

Ainsi:

``` py

T = [10, 2, 8, 205, -1, 0]
print(T[0]) # affiche le premier élément de la liste T, soit 10
print(T[4]) # affiche le cinquième élément de la liste T, soit -1
print(T[-1]) # ASTUCE: affiche le dernier élément de la liste T, soit 0
```
Ce sont des opérations de **lecture** puisque le ``print`` permet de lire différentes valeurs de la liste.

!!! warning "Index out of range"
    Attention, si le tableau contient ```n``` éléments, les indices vont alors de ```0```(le premier) à ```n-1```(le dernier). 

Si vous donnez un indice en dehors de cette plage vous, retourverez la fameuse exception levée par python:
<center>
![](img/indexout.png){ width=50% }
</center>


## Accès à tous les éléments d'une liste

!!! info "Accès à tous les éléments de la liste(opération de lecture)"

    On peut accèder aux valeurs de tous les éléments de la liste par une **boucle.**

Il existe deux façons de procéder proposées dans le code suivant:

``` py
T = [10, 2, 8, 205, -1, 0]
#attention, il faut que la liste T existe!!!
# METHODE 1 : On parcourt sur les indices
#i correspond à l'indice et T[i] à la valeur de l'élément situé au rang i
for i in range(len(T)): 
    print(T[i])#affiche tous les éléments de la liste
# METHODE 2 : On parcourt les éléments
# elt prend successivement comme valeur tous les éléments de la liste
for elt in T:
    print(elt)#affiche aussi tous les éléments de la liste

```

!!! exo "Affichage dynamique"
    === "Exercice"
        Utiliser une ```f-string``` pour faire afficher dans la console ```L'élément de la liste situé au rang 0 est 10```, ```L'élément de la liste situé au rang 1 est 2```, etc...
    === "Correction"
        ```py
        T = [10, 2, 8, 205, -1, 0]
        for i in range(len(T)):
            print(f"L'élément de la liste situé au rang {i} est {T[i]}.")
        ```

Une liste étant donnée, on peut aussi la modifier: ce sont alors des opérations d'**écritures**.

!!! info "Modification des éléments de la liste(opération d'écriture)"

    On peut modifier les valeurs des éléments de la liste par la connaissance de son rang, son indice en réaffectant la valeur souhaitée.
  
Ainsi:

``` py

T = [10, 2, 8, 205, -1, 0]
T[0] = 4 #modifie la première valeur du tableau
T[1] = T[2] #la seconde valeur devient la troisième!
```
Rappelons au passage qu'on peut aussi accèder aux éléments d'une chaîne de caractères mais qu'on ne peut pas la modifier: on dit que cet objet est **non mutable**.

!!! info "Ajout d'un élément à la liste(opération d'écriture')"

    On peut **ajouter** une valeur à la fin d'un tableau( à sa droite...) par la méthode ```append()```.

Par exemple, ajoutons la valeur 100 au tableau initial ``T``:

``` py

T = [10, 2, 8, 205, -1, 0]
T.append(100)# la liste est alors [10, 2, 8, 205, -1, 0, 100]
```
## Génération de listes

!!! info "Construction d'une liste par compréhension"

    Python permet la construction de liste par **compréhension**. Le principe est donné dans les exemples suivants.

``` py
from random import randint
#construction par compréhension d'une liste de 10 entiers aléatoires compris entre 0 et 100
# pour chaque valeur de i, un entier aléatoire entre 0 et 100 est généré.
entier_alea = [randint(0, 100) for i in range(10)]
#construction par compréhension de la liste des carrés des 10 premiers entiers 
carre = [i**2 for i in range(10)]
```
<h2 class = "python"><span class="fa-3x">
  <i class="fa-solid fa-marker fa-flip fa-xs" style="--fa-animation-duration: 5s;" ></i>
  
</span> Exercices corrigés</h2>

!!! exo "Les instructions de bases."
    === "Exercice"
        1. Créer la liste ``` T = [10, 2, 8, 205, -1, 0] ``` .
        2. Faites afficher dans la console, sa longueur, son premier terme, son dernier.
        3. Donner l'instruction python qui permet d'ajouter ```16``` à la fin de la liste.
        4. Donner l'instruction python qui permet de changer l'élément de ```T``` situé au rang 2 en ```14```.
        5. Donner l'instruction python qui permet d'affecter au premier terme de la liste la valeur du dernier terme... quel que soit la liste!
        6. Donner les instructions qui permettent d'ajouter 1 à **tous** les éléments de la liste.
    === "Correction"
        ```py
        T = [10, 2, 8, 205, -1, 0]
        print(f"La longueur de la listes est {len(T)}")
        print(f"Son premier terme est {T[0]}.")
        print(f"Son dernier terme est {T[-1]}")
        T.append(16)
        T[2] = 14
        T[0] = T[-1]
        #Il faut parcourir toute la liste d'où la boucle
        for i in range(len(T)):
            T[i] = T[i] + 1
        #On peut faire un print final pour observer toutes les modifications
        print(T)
        ```

!!! exo "Comptage"
    === "Exercice"
        1. Créer par compréhension une liste de 65 entiers choisis au hasard entre ``0`` et ``1000``.
        2. Déterminer les instructions Python qui permettent de compter les nombres pairs de cette liste.
    === "Correction"
        ```py
        from random import randint
        ma_liste = [randint(0, 1000) for i in range(65)] # liste par compréhension
        cpt = 0
        #un nonbre est pair si le reste de la division euclidienne par 2 est nul
        for nombre in ma_liste:# pour parcourir toute la liste
            if nombre%2 == 0: #test de parité
                cpt = cpt + 1 #incrémentation du compteur
        print(cpt)
        ```

!!! exo "À l'envers!"
    === "Exercice"
        1. Créer une liste de 20 entiers choisis au hasard entre ``0`` et ``50``.On utilisera la méthode ``append``.
        2. Déterminer les instructions Python qui permettent de créer une nouvelle liste contenant les mêmes nombres mais à l'envers(le premier devient le dernier, le deuxième l'avant dernier...)
    === "Correction"
        ```py
        from random import randint
        ma_liste = [] #liste vide au départ
        for i in range(20):
            ma_liste.append(randint(0, 50))
        # à chaque tour de boucle j'ajoute à la liste un nouveau nombre
        
        liste_ma = [] #nouvelle liste vide
        for i in range(len(ma_liste)):# pour parcourir la liste ma_liste
            liste_ma.append(ma_liste[len(ma_liste) - 1 - i])
        ```

C'est un classique des listes: lorsque la variable ``i`` augmente de ``0`` à ``len(ma_liste) - 1``, la variable ``j = len(la_liste) - 1 - i`` varie de ``len(ma_liste) - 1`` à ``0``.

!!! exo "Valeur maximale"
    === "Exercice"
        Une liste ``L `` étant donnée, déterminer les instructions qui permettent de déterminer le plus grand élément de la liste (interdit d'utiliser le mot réservé ``max``).
    === "Correction"
        ```py
        from random import randint
        L = [randint(0, 1000) for i in range(100)] # liste par compréhension pour le test
        element_max = L[0] # je suppose que le premier est le plus grand
        for i in range(len(L)):
            if L[i] > element_max:
                element_max = L[i]
        print(element_max)
        ```

!!! exo "Comprendre la compréhension"
    === "Exercice"
        Chaque liste ci-dessous est générée par la méthode de compréhension, propre au langage Python.
        Déterminer dans chaque cas la valeur demandée.
        
        1. Que vaut ``L[0]`` ,``L[5]`` et ``L[10]`` si ``L`` est la liste ci-dessous:
        ```py
        L = [2*i+3 for i in range(10)]
        ```
        2. Que vaut ``M[1]`` ,``M[4]`` et ``M[-1]]`` si ``M`` est la liste ci-dessous:
        ```py
        M = [elt**2-1 for elt in L]
        ```
    === "Correction"
        1. La liste ``L`` vaut ``[3, 5, 7, 9, 11, 13, 15, 17, 19, 21]`` donc ``L[0]=3``, ``L[5]=13`` et ``L[10]`` n'existe pas car le dernier indice de la liste est ``9``.
        2. ``elt`` itère sur ``L`` ce qui veut dire que ``elt`` vaut ``3``, puis ``5``,... jusqu'à ``21``. Donc ``M =[8, 24, 48, ..., 440]``. Donc ``M[1]= 24``, ``M[4]=120`` et ``M[-1]`` est le dernier de la liste soit ``440``
        

<h2 class = "python"><span class="fa-3x">
  <i class="fa-solid fa-wrench fa-flip fa-xs" style="--fa-animation-duration: 5s;" ></i>
  
</span> Exercices</h2>

!!! exo "Exercice: "
     Donner dans le terminal ci-dessous, les instructions ```python``` qui permettent:

    - de créer une variable ```L```, de type ```list``` et de lui affecter les valeurs ```1,8,2,20,5,40``` et ```0```.
    - de changer la valeur de l'élément de ```L``` situé au rang 2 en lui attribuant la valeur -1
    - d'ajouter la valeur 50 à la fin de la liste ```L```. 
    - de modifier tous les éléments de la liste ```L``` en les multipliant par 2.
    - de construire une nouvelle liste ```L1``` dont tous les éléments sont ceux de la liste ```L``` auquels on a ajouté 1.

    
{{ terminal() }}

!!! exo "Exercice: "
    Donner les instructions ```python``` qui permettent de construire une liste de 100 entiers aléatoires entre 1 et 6 avec la méthode ```append``` d'abord puis par compréhension ensuite.

!!! exo "Exercice: "
    Modifier une liste selon le principe suivant : on ajoute à l’élément situé au rang i son
    élément précédent s’il existe. Par exemple si la liste est ``[2, 5, 6, 1]`` alors la nouvelle liste serait
    ``[2, 7, 11, 7]``.

!!! exo "Exercice: "
    Donner des instructions qui permettent de réaliser une permutation circulaire des éléments d’une liste. Les exemples suivants expliquent la manipulation.
    ```py
    L = [1, 2, 3, 4, 5] #liste initiale
    L = [5, 1, 2, 3, 4] #liste après une permutation circulaire
    L = [4, 5, 1, 2, 3] #liste après deux permutations circulaires
    ```



!!! exo "Exercice:"
    Dans cet exercice, il s'agit de construire un nuage de points de coordonnées ```(liste_abscisse, liste_ordonnee)``` où ```liste_abscisse```(resp. ```liste_ordonnee```) est une liste contenant les abscisses (resp. ordonnées) des points.

    1. Copier le code suivant et exécutez-le:
    ```py
    import matplotlib.pyplot as plt

    x = 0
    pas = 0.05
    nombre_de_points = 1000
    liste_abscisse = []
    liste_ordonnee = []
    for i in range(nombre_de_points):
        x = x + pas
        y = 0.5*x - 2
        liste_abscisse.append(x)
        liste_ordonnee.append(y) 
        
    plt.plot(liste_abscisse, liste_ordonnee)#plot pour courbe
    plt.title(f"pas:{pas}, nombre de points: {nombre_de_points}")
    plt.show() # affiche la figure à l'écran
    ```
    2. De quelle fonction mathématique avons-nous tracé la courbe représentative?

    3. Modifier le code pour construire la courbe représentative de la fonction $f$ définie pour tout $x$ dans $[0;5]$ par $f(x)=0.5x^2-1$

!!! exo " Simuler le lancer d'un dé"
    On souhaite simuler plusieurs lancers d'un même dé.

    1. Donner l'instruction qui permet d'affecter à une variable ```n```, un entier saisi au clavier.
    2. Donner l'instruction qui permet d'affecter à une variable ```de``` un nombre entier aléatoire entre 1 et 6.
    3. Donner les instructions ```python``` qui permettent de simuler ```n``` lancers d'un dé à six faces et de stocker les résultats dans une liste ```resultat```.
    4. Plus dur! Donner les instructions ```python``` qui permettent de calculer la fréquence de sortie de chaque face lorsque on jette ```n``` fois un dé.

!!! abstract "Ce qu'il faut retenir"
    - Il faut savoir créer, définir des listes:  à la main, par compréhension, avec la méthode ``append``.
    - Il faut savoir extraire des informations sur les listes: leur longueur, l'élément situé à l'indice ``i``, son premier élément, son dernier, ...
    - Il faut savoir modifier les éléments d'une liste: par exemple ajouter ``2`` à tous les éléments, multiplier par ``4`` ceux situés à des rangs pairs,...

