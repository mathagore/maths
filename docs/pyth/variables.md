
<link rel ="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.7.2/css/all.min.css">

<h2 class = "python"><span class="fa-3x">
  <i class="fa-solid fa-user-graduate fa-flip fa-sm" style="--fa-animation-duration: 5s;"></i>
  
</span> Des variables pour stocker de l'information</h2>
!!! info "De quoi s'agit-il?"

    Un ordinateur est une grande commode composée de millions de tiroirs dans lesquels on peut stocker des données!

Je fais référence aux espaces de stockages des ordinateurs de la plus grande (le disque dur) à la plus petite (le registre), en passant par les mémoires vives par lesquelles transitent les informations.

En informatique , tout est **donnée numérique**: les pages web, les images, les sons, les mels ,... et même les programmes sont des données stockées dans des variables contenant des valeurs de différents types. D'où notre obsession sur les **variables**...

On verra comment des **algorithmes** implémentés par des **programmes** peuvent changer l'état de ces variables...

<center>
  ![](../img/commode.png){ width=30% }
</center>

!!! note "Gardez en tête!"

    Une variable a un **nom**, un **type** et une **valeur**

C'est essentiel: le type de variables utilisées induit les opérations permises sur ces variables... 

!!! exemple "Exemples de type de variables"

    - La variable ``a = 150 `` est de type ``int``(entier) et sa valeur est ``150``
    - La variable ``nbre = 2.56`` est de type ``float``(flottant) et sa valeur est ``2.56``
    - La variable ``choix = "Mardi"`` est de type ``str``(string) et sa valeur est ``Mardi``
    - La variable ``test = a == 100`` est de type ``bool``(booléen) et sa valeur est ``False`` (car ``a`` ne vaut pas ``100``).


## Créer des variables

``Python`` est un langage informatique qui permet de donner des instructions. Par exemple, le code suivant:

``` py
a = 2
b = 10
c = a + b
```
crée trois variables  ` a,b` et ` c`. Sans trop de détails, vous venez simplement de réserver trois tiroirs de votre gigantesque commode, en leur donnant des noms et des valeurs.

!!! info " Vocabulaire"
    Lorsqu'on crée des variables et qu'on leur attribue des valeurs, on dit qu'on **affecte** une variable ou simplement que l'on a fait une  **affectation** de variables. On peut aussi parler d'**initialisation** de la variable quand pour la première fois, on affecte une valeur à une variable.

En ` Python`, chaque variable a un **type** qui peut-être, un nombre entier ou à virgule (en `python`, on dit un `int` ou un `float`), un texte (en `python`, on dit un `string`) , une liste (en `python`, on dit une ... `list`) ou tout autre chose dont on ne parlera pas pour le moment...

!!! question "Pourquoi stocker dans des variables???"
    Tout simplement pour garder en mémoire(celle de l'ordi), les résultats obtenus ou calculés par un programme. Il suffit d'invoquer le nom de la variable pour retourver son contenu!

Un exemple! Le programme suivant calcule le prix ```TTC``` d'un article à partir du prix ```HT``` et de la valeur du taux d'imposition lié à la ```TVA```. Vous remarquerez que les variables utilisées sont ```implicites```: une variable qui désigne un prix sera appelée ```prix``` ou ```p```, mais pas ```x``` !!

``` py
prix_HT = 200
taux = 1.196 #pour une TVA a 19.6%
print(prix_HT * taux)
```

Le programme calcule puis **affiche** le prix ```TTC``` dans la console ```python```. Mais le résultat **ne sera pas conservé** en mémoire! On peut (doit?) remédier à cela en créant la variable ```prix_TTC``` et l'affecter comme suit:
``` py
prix_TTC = prix_HT * taux
```
et faire ensuite afficher le contenu de cette variable:
``` py
print(prix_TTC)
```

## Saisir des valeurs au clavier

La méthode ``print`` permet d'afficher la valeur des variables. Nous allons maintenant utiliser l'instruction ``input`` qui permet de communiquer une valeur d'une variable au programme:

```py
prenom = input("Donner votre prenom: ")
```
!!! warning "Encore un souci de type"
    Lorsque vous utilisez l'intruction ``input`` , la donnée stockée dans la variable  est alors automatiquement de type ``str``!

En effet même si vous saisissez un nombre au clavier, il sera considéré comme une chaîne de caractères et non pas comme un ``int`` ou un ``float``.

Si vous voulez stocker une information de type ``int`` ou un ``float`` il faut alors l'indiquer comme suit:

```py
age = int(input("Donner votre âge: ")) # pour saisir des entiers
taille = float(input("Donner votre taille: ")) # pour saisir des flottants(décimaux...)
```






## Les opérateurs sur les différentes variables

Il existe des **opérateurs** qui peuvent modifier l'état de ces variables. Outre les opérations mathématiques usuelles $(+, -, \times, \div)$, on aura l'occasion d'en découvrir d'autres.

!!! exo "Donner la valeur des variables ` a,b` et ` c` à la suite de ces affectations."

    === "Code"

        ``` py
        a = 2
        b = 10
        c = a + b
        b = b + 1
        a = a - 2
        c = c**2
        ```


    === "Solution"

        ``` py
        a = 2
        b = 10
        c = a + b # c vaut alors 12
        b = b + 1 # b vaut 11
        a = a - 2 # a vaut 0
        c = c**2  # c vaut le carré de 12 soit 144
        ```


Il faut lire l'instruction `b = b + 1` ainsi : la nouvelle valeur de la variable `b` est égale à la valeur de l'ancienne à laquelle j'ajoute 1. Cette opération porte même un joli nom:

!!! info " Vocabulaire"
    L' opération qui consiste à faire évoluer une variable en lui ajoutant 1 s'appelle une **incrémentation**.

Cette opération d'incrémentation est très utilisée en programmation. 

!!! exo "Une instruction très utilisée..."
    
    === "Questions"

        1. Quelle instruction Python fait diminuer de 1 une variable `a` ?
        2. Cherchez le nom de cette instruction.
        3. Donnez le code qui incrémente une variable ```compteur``` de 5. 

    === "Réponses"

        1. `a = a - 1 #attention dans ce cas il faut que `a ` est été initialisée!`
        2. C'est la **décrémentation**!
        3. ```compteur = compteur + 5```

Rappelons aussi le **modulo** et la **division entière**, découvert dans le chapitre précédent.


On peut aussi jouer avec des lettres ou du texte, ce que nous appelons des variables de type ```chaînes de caractères``` ou simplement `string` en python.

``` py
nom = "Alfred" #type string
club = "Madrid" #type string
age = "30" #type string même si il est écrit 30!!
but = 26 #type int
taille = 1.78 #type float
```
Les variables de type `string` se déclare avec des **quotes**, doubles `" "` ou simples ` ' ' ` (la tradition veut que l'on utilise les simples pour un seul caractère...): les variables `nom, club` et `age` sont ainsi des chaînes de caractères. La variable `but` est en revanche un entier de type `int` et la variable ``taille`` de type ``float``.

!!! warning "Attention!"
    Rappelons qu'une variable a un **type**, défini dans la façon dont la variable a été déclarée! Des **opérations** existent selon le type de la variable.

On peut pour illustrer mes propos, utiliser les **opérations** mathématiques classiques sur les variables de type nombres (```int``` ou ```float```) mais pas sur les chaînes de caractères!

La variable `age` n'est donc pas une variable de type `int` mais bien `string` car déclarée avec des _quotes_!

Dans le cadre de ce cours, nous ne parlerons pas des opérateurs qui agissent sur les chaînes de caractères et les booléens.


## Générer du hasard

Dans de nombreux cas, nous aurons besoin de simuler le hasard: plus précisément, certaines variables devront être affectées avec une **valeur aléatoire**. On distingue en général, les valeurs **entières** des valeurs **décimales**.

Pour cela, il faut **importer** la bibliothèque qui génére les nombres aléatoires et surtout la méthode ```randint```en écrivant le code suivant:

```py
from random import randint
```
Puis pour simuler le lancer d'un dé par exemple, on pourra écrire:
```py
de = randint(1, 6) #la valeur de la variable de est un entier compris entre 1 et 6
```

Cette  instruction n'importe **que** la méthode ```randint``` : la bibliothèque ```random``` en contient d'autres que nous n'utiliserons pas ici!

!!! exercice "Jouer avec le hasard"
    Copier le code suivant dans la zone de script de Thonny et exécutez le!
    ```python
    from random import randint # Importation de la méthode randint de la bibliothèque random  
    n = int(input("Proposez un nombre entre 0 et 100")) # Affectation de la variable n par l'utilisateur
    choix_ordi  = randint(0, 100) # Affectation de la variable choix_ordi par l'ordinateur
    if n == choix_ordi: # Comparaison des valeurs des variables n et choix_alea
        print("Quelle chance vous avez!!!")
    else: # else = sinon !!
        print("Et non, c'est raté!!")

    ```

Quelques précisions:

1. L'instruction ```input``` permet à l'utilisateur de saisir une information au clavier.
2. L'instruction ```int``` assure que la valeur saisie au clavier est bien un entier(par défaut c'est une chaîne de caractères...). Si vous souhaitez saisir un nombre décimal, il faut alors précéder l'instruction non pas par ```int``` mais ```float```.
3. L'instruction ```randint(0, 100)``` permet de générer un **entier aléatoire** entre 0 et 100.
3. L'instruction ``==`` permet de **comparer** la valeur des deux variables ``n`` et ``choix_ordi``.
4. Les instructions ```if``` et ```else``` définissent une structure conditionnelle. On peut multiplier les cas en ajoutant des ``elif``.

!!! exo "Compléter le code précédent"
    1. Modifier le code précédent en ajoutant les instruction suivantes:
    ```python
    if n == choix_ordi:
        print("Quelle chance vous avez!!!")
    elif n > choix_ordi:
        print("Et non, c'est moins!!")
    else:
        print("Et non, c'est plus!!")
    ```
    2. Testez votre code.



<h2 class = "python"><span class="fa-3x">
  <i class="fa-solid fa-marker fa-flip fa-xs" style="--fa-animation-duration: 5s;" ></i>
  
</span> Exercices corrigés</h2>

!!! exo "Trouver la valeur"

    === "Exercice"
        Donner la valeur des variables suivantes à la fin de l'exécution de ce code
        ```py
        nbre1 = 5
        nbre2 = nbre1**2
        q = nbre2 // 7
        r = nbre2 % 7
        test = nbre2 == 25
        nbre1 = nbre1 - 2

        ```
    === "Correction"
         ``nbre2 = 25 ``,  ``q = 3 `` ,  ``r = 4 ``,  ``test = True ``, ``nbre1 = 3 ``.
    
!!! exo "Déclarer des variables"

    === "Exercices"
        1. Donner les instructions ```python``` qui permettent:

            - de créer une variable ```h``` et de lui affecter la valeur ```10```.
            - de créer une variable ```p``` et de lui affecter le texte ```bonjour```.
            - de créer une variable ```prix``` et de lui affecter la valeur ```12.5```
        2. Tapez ces instructions dans la console ci-dessous puis pour chaque variable faites affichez son type par l'instruction ```print(type(var))``` où ```var``` doit être remplacé par les noms des variables créées précédemment.
        3. Donner les instructions ```python``` qui:
            - ajoute 8 à la variable ```h``` et stocke le résultat dans ```h```.
            - multiplie la variable ```prix``` par ```1.1``` et stocke le résultat dans ```prix```
    === "Correction"
        1. ``h = 10``, ``p = "bonjour"``, ``prix = 12.5``
        2. ``print(type(h))`` puis ``print(type(p))`` et ``print(type(prix))``. Vous trouverez donc les types ``int``, ``str`` et ``float``.
        3. ``h = h + 8``, ``prix = prix*1.1``


!!! exo "Conversion"
    === "Exercice"
        1. Écrire un programme qui permet à l'utilisateur de saisir un coefficient multiplicateur et qui affiche l'évolution correspondante en pourcentage.
        2. Écrire un programme qui permet à l'utilisateur de saisir un angle en degré et affiche sa valeur en radian dans la console.
    === "Correction"
        1. On passe du coefficient au pourcentage en enlevant 1 puis mulitipliant par 100.
        ```py
        # coef peut être décimal, j'utilise donc un float
        coef = float(input("Donner le coefficient multiplicateur"))
        pourcentage = (coef - 1)*100
        # j'utilise un f-string mais vous pouvez juste taper print(pourcentage)
        print(f"Le pourcentage d'évolution correspondant au coefficient {coef} est {pourcentage}%")
        ```
        2. On passe des degrés en radians en multipliant l'angle en degré par ``pi``(3.14 ici) et en divisant par 180.
        ```py
        # je ne considère ici que des angles de valeurs entières, sans décimales.
        angle_degre = int(input("Donner votre angle en degré(Valeur entière obligatoire): "))
        #/ est un division décimale et // une division entière, ce n'est pas la même chose.
        angle_radian = angle_degre*3.14/180 
        print(angle_radian)
        ```
        

<h2 class = "python"><span class="fa-3x">
  <i class="fa-solid fa-wrench fa-flip fa-xs" style="--fa-animation-duration: 5s;" ></i>
  
</span> Exercices</h2>






!!! exo "Conversion bis"
    3. Écrire un programme qui permet à l'utilisateur de saisir un nombre et qui affiche son carré. Attention, le nombre saisi peut éventuellement être un nombre décimal.
    4. Le TURRO est une unité métrique imaginaire correspondant à ``1.87m``.Écrire un programme qui permet à l'utilisateur de saisir une taille donnée en mètres, et affiche sa conversion en TURRO.



!!! exo "Des instructions simples mais faut aussi réfléchir un peu"
    1. Donnez des instructions qui permettent de saisir son âge et qui affiche votre année de naissance.
    2. Donnez des instructions qui permettent de saisir la largeur et la longueur d'un rectangle et qui affiche son aire.
    3. Donnez des instructions qui permettent de saisir votre prénom et une note et qui affiche par exemple ``Je m'appelle Henri et j'aimerai avoir 18 en Maths`` ou ``Je m'appelle Maria et j'aimerai avoir 4 en NMaths``...
    4. Même chose mais les notes sont choisies de façon aléatoire entre ``0`` et ``20``.







!!! exo " Concevoir un programme avec des conditions multiples "
    Écrire un programme python qui permet à l'utilisateur de saisir une note et d'afficher dans la console:

    - ```TB``` si la note est plus grande ou égale à 16
    - ```B``` si la note est plus grande ou égale que 14
    - ```AB``` si la note est plus grande ou égale à 12
    - ```Reçu``` si la note est plus grande ou égale à 10
    - ```Refusé``` sinon

!!! exo "Un exercice complet"
    1. Créer une variable ``nombre1`` à laquelle tu affectes un nombre aléatoire entier entre 1 et 9.
    2. Créer une variable ``nombre2`` à laquelle tu affectes un nombre aléatoire entier entre 1 et 9.
    3. Créer un affichage à l'écran ``Combien vaut le produit de nombre1 x nombre2?`` où les variables ``nombre1`` et ``nombre2`` sont remplacées par leur valeur(utilise une f-string).
    4. Récupère la réponse de l'utilisateur et stocke-là dans une variable ``reponse``.
    Si la réponse est correcte fait afficher ``Bravo!`` sinon fait afficher ``Perdu! La bonne réponse était...`` .




!!! abstract "Ce qu'il faut retenir"
    - Il faut savoir qu'une variable a un **nom**, un **type** et une **valeur**.
    - Il faut connaître les instructions(incrémentation, décrémentation,...) et les opérateurs (opérations mathématiques en particulier...) qui modifient la valeur de ces variables
    - Il faut être capable d'écrire une structure conditionnelle.