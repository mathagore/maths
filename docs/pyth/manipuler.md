<link rel ="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.7.2/css/all.min.css">

<h2 class = "python"><span class="fa-3x">
  <i class="fa-solid fa-user-graduate fa-flip fa-sm" style="--fa-animation-duration: 5s;"></i>
  
</span> Je fais du python et j'aime ça!</h2>


Voici ci-dessous, un **IDE**(Integrated Development Environment) intégré à la page web: 



- d'abord une **zone de script** contenant du code Python, 
- en dessous le **terminal** qui permet de communiquer avec l'utilisateur. 

Python est un langage interprété: lorsque vous exécutez le script, ce langage interprète ligne par ligne les instructions...


!!! exercice "Échauffement"
    Premier exercice très difficile: exécutez le code( à vous de trouver le bon bouton...) et lisez le résultat dans le terminal...


{{ IDE('intro_variable') }}


Quelques explications!

1. L'instruction ```a = 10``` permet d'affecter la valeur ```10``` à la variable ```a```.
2. L'instruction ```type(a)``` permet de renvoyer le type de la variable ```a```. En ce qui nous concerne les types seront ```int, float, list, str, boolean``` correspondant aux entiers, flottants(le décimaux...), listes (ou tableaux), les chaînes de caractères et booléen!(précision dans le prochain... cours)
3. L'opérateur **division entière** (```a//b```), donne le quotient entier de ```b``` dans ```a```, c'est-à-dire combien de fois on peut mettre ```b ``` dans ```a```.(exercice un peu plus loin...)
3. L'opérateur **modulo** (```a%b```), donne le reste  de la division euclidienne de  ```a``` par ```b```, c'est-à-dire ce qu'il reste lorsque qu'on a enlevé  ```(a//b)*b``` à ```a``` :fontawesome-regular-face-laugh-wink: (exercice un peu plus loin...)
3. L'instruction ```print(...)``` permet d'afficher dans la console la valeur de la variable passée en paramètre de la méthode ```print```...
4. L'instruction ```f"La variable {a} est du type {type(a)}..."``` est une **```f-string```**, une chaîne de caractère **formatée** (d'où le **f** devant) par la valeur de ```a``` et ```type(a)```.

!!! info "Afficher dans la console"

    1. Pour afficher la **valeur** d'une variable ``v`` quelconque dans la **console** ou le terminal, on saisit l'instruction ``print(v)``.

    2. Pour afficher un texte, ``Salut!`` par exemple, on saisit ``print("Salut!")``. Remarquez alors la présence des doubles quotes ``" "``.


Les ```f-string``` sont très utiles pour faire des affichages dynamiques. Nous en utiliserons énormément...


!!! exercice "Afficher dans la console"
    === "Exercices"
        1. Dans le script précédent, faites afficher la valeur de la variable ```c``` puis celle de ```d``` dans la console.
        2. Utiliser une ```f-string``` pour obtenir par exemple, ```La valeur de c est 25```.
        
    === "Correction"
        1. Il suffit d'ajouter ``print(c)`` et ``print(d)``. 
        2. ```print(f"La valeur de c est {c} et celle de d est {d}")```



!!! info "À savoir"
    On utilise souvent un IDE complet avec sa zone de script pour faire du python. Mais on peut utiliser seulement le terminal comme ci-dessous.

Le bloc des trois chevrons ```>>>``` est appelé **prompt** ou **invite de commandes**(c'est plus fun...)

!!! exo "Utiliser le terminal pour faire des calculs"
    === "Exercice"
        1. Tapez l'instruction ```51//4``` après le prompt et validez avec la touche <kbd>Enter</kbd>
        2. Tapez l'instruction ```51%4``` après le prompt et validez avec la touche <kbd>Enter</kbd>
        3. Tapez l'instruction ``2**8``. Que fait l'opérateur python ``**`` ?
    === "Correction"
        1. Vous trouverezz ``12`` car on peut mettre ``12`` fois ``4`` dans ``51``($12\times 4 = 48$ et $13\times 4 = 52$.)
        2. Vous  trouverez ``3`` car quand vous avez enlevé le plus grand multiple de ``4`` dans ``51`` il reste ``3``...
        3. C'est la syntaxe qui permet de calculer la puissance (ici $2^8$).
{{terminal() }}

!!! exercice "Division euclidienne"
    1. Déterminer avec votre copain de droite (ou de gauche), la valeur des instructions suivantes: ```10//2``` et ```10%2``` . Vérifier avec python en utilisant le terminal ci-dessus.
    2. Même question avec les instructions ```86//3``` et ```86%3```, puis ```100//17``` et ```100%17```. Vérifier avec python en utilisant le terminal ci-dessus.
    3. Calculer $2^{10}$ et $1.05^5$ avec le terminal ci-dessus.


!!! note "À retenir"
    Python permet de réaliser les opérations mathématiques de base $(+,-,\times,\div)$ et les puissances. À cela s'ajoutent d'autres opérateurs comme le ``//``(division entière) ou le ``%``(modulo).

Les opérateurs ```//``` et ```%``` sont très utiles. Il faut savoir les utiliser et être capable de les calculer dans des cas simples.

## Un vrai IDE: Thonny

Nous allons maintenant utiliser un vrai IDE dédié à l'écriture de script Python: ce logiciel s'appelle **Thonny** et est installé sur le réseau du lycée.

Principalement, et quelque soit le logiciel utilisé, il existe:

- la zone d'édition ou de script qui permet d'écrire et de l'éxecuter par la suite. Avantage: on peut conserver son code dans un fichier dont l'extension sera `.py`.
- la **console** ou le **terminal** qui permet de donner directement des ordres

Ci-dessous, l'environnement proposé par le logiciel _Thonny_:

<center>
  ![](../img/presentation_thonny.png){width=60%}
  </center>
 Nous allons donc dorénavant utiliser ce logiciel. Mais avant cela, quelques précisions...

## Bien écrire du python!

Python est un langage de haut niveau (c'est-à-dire que sa syntaxe est proche du langage humain 😀) qui possède ses régles d'écriture décrites dans la [PEP-8](https://peps.python.org/pep-0008/).

En particulier, on respectera les règles suivantes:

 - On entoure d'espaces le signe `=` lors de l'affectation de variables
 - On entoure d'espaces tous les signes d'opérations `+,-,*,...`

C'est juste un confort visuel et le code s'éxcuterait tout de même sans le respect de ces consignes.

!!! example "À faire ou ne pas faire"

    === "BIEN"

        ``` py
        a = 1
        b = 2
        nom = "Hector"
        if x > 2:
            x = x + 2
        ```

    === "PAS BIEN"

        ``` py
        a=1
        b=2
        nom="Hector"
        if x>2:
            x=x+2
        ```

Autre information importante, source de nombreuses erreurs!

!!! info "L'indentation"
    L'**indentation** est le retrait de 4 espaces vers la droite de certaines instructions. Elle permet de définir les blocs d'instructions dans les conditions, les boucles ou les fonctions.

Dans d'autres langages informatiques, les blocs d'instructions sont mis entre accolades(```{}```), pas en python:

 Dans la **structure conditionnelle** précédente, les instructions sont indentées. Remarquez au passage la présence des ```:``` dont l'oubli provoque des erreurs...

!!! example "Visualisation des blocs indentés"

    ```py
    n = 19%7
    if n == 5:
        print("Le nombre vaut 5")
    else:
        print("Le nombre ne vaut pas 5")
        print("Fin!")
    ```
Si la valeur de la variable ``n`` est égale à ``5`` alors un seul ``print`` est effectué sinon deux ``print`` sont réalisés!





## Gérer ses erreurs

Python est prévoyant: il anticipe toutes les erreurs syntaxiques que vous pourriez commettre!

- Une erreur d'indentation et il vous annonce dans la console: ```IndentationError: expected an indented block after ...```
- un oubli des deux points ```:``` après des instructions et il vous annonce dans la console: ```SyntaxError: expected ':'```
- une faute d'orthographe dans l'appel d'une méthode, par exemple ```imput``` au lieu de ```input``` et il vous annonce dans la console: ```NameError: name 'imput' is not defined ```

!!! danger " MESSAGE D 'ERREUR!!!"
    Donc avant d'appeler votre professeur au moindre petit problème, regardez le message dans la console et corriger par vous même!!!




<h2 class = "python"><span class="fa-3x">
  <i class="fa-solid fa-marker fa-flip fa-xs" style="--fa-animation-duration: 5s;" ></i>
  
</span> Exercices corrigés</h2>
    
!!! exo "Corriger les erreurs"
    === "Exercice"
        Vous trouverez ci-dessous quatre scripts(programmes...) qui lors de leur exécution, produisent une erreur. Corrigez cette erreur en utilisant s'il le faut les indications données dans la console.
        ```py
        print(Je fais du python)
        ```
        ```py
        print(mesure)
        ```
        ```py
        pint("Nous sommes un lundi!")
        ```
        ```py
        n = 105
        if n % 10 == 0:
        print("n est un multiple de 10" )
        else:
        print(" n n'est pas un multiple de 10")
        ```
    === "Correction"
        1. Si vous voulez affichez du texte dans la console avec la méthode ``print``, il faut mettre le texte entre des quotes ``"..."``.
        ```py
        print("Je fais du python")
        ```
        2. Vous demandez à Python qu'il vous affiche la valeur d'une variable ``mesure`` qui n'est pas définie donc erreur... Je vous rappelle que si vous souhaitez affichez le texte ``mesure`` alors on doit taper ``print("mesure")``!
        2. Le mot ``pint`` est inconnu, d'ailleurs il n'a pas "la bonne couleur" dans le script. Nous commettons souvent des fautes d'orthographes que Python ne pardonne pas...
        ```py
        print("Nous sommes un lundi!")
        ```
        2. Dans les conditions et les boucles, les blocs d'instructions sont **indentés** de quatre espaces (ou une tabulation). D'ailleurs quand vous tapez ces instructions, il y a naturellement un retour à la ligne indenté quand vous validez après le ``:``.
        ```py
        n = 105
        if n % 10 == 0:
            print("n est un multiple de 10" )
        else:
            print(" n n'est pas un multiple de 10")
        ```
    


<h2 class = "python"><span class="fa-3x">
  <i class="fa-solid fa-wrench fa-flip fa-xs" style="--fa-animation-duration: 5s;" ></i>
  
</span> Exercices</h2>




!!! abstract "Ce qu'il faut retenir"
    - Il faut savoir utiliser les ``print`` pour communiquer avec la machine.
    - Il faut être capable d'utiliser le terminal pour faire des calculs mathématiques.
    - Il faut connaître les opérateurs ``//`` et ``%`` de la division euclidienne.
    