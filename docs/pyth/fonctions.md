<link rel ="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.7.2/css/all.min.css">

<h2 class = "python"><span class="fa-3x">
  <i class="fa-solid fa-user-graduate fa-flip fa-sm" style="--fa-animation-duration: 5s;"></i>
  
</span> Comprendre les fonctions</h2>


Le code suivant:

``` py 
L = [8, 5 , 6 , -1, 45, 0, 20]
som = 0
for i in range(len(L)):
    som = som + L[i]
print(som)
```
donne les instructions qui permettent de calculer la somme des éléments d'une liste ```L``` quelconque. 

Mais finalement ce code ne change pas si on change la liste ```L```: il aura toujours la vocation à calculer la somme des éléments de la liste proposée en préambule.... Des exemples:


<figure markdown>
  ![](img/fctintro.png){ width="400" }
  <figcaption> On visualise le côté transformation des fonctions mathématiques</figcaption>
</figure>

On a donc :

  - en **entrée** une liste Python
  - en **sortie** un nombre (entier ou décimal...)

L'idée qui nous intéresse est de reprendre le modèle mathématique des fonctions qui transforment une donnée en une autre. Un programme $f$ qui ferait ceci:

<center>
$f(Liste) = Somme$
</center>

La syntaxe Python qui crée **une fonction** est donnée par le code ci-contre:

``` py 
def somme_de_liste(une_liste):
    n = len(une_liste)
    som = 0
    for i in range(n):
        som = som + une_liste[i]
    return som
```

Cette fonction ```Python```:

  - est une fonction ```Python``` car est déclarée avec la particule ```def```;
  - porte le nom **implicite** de ```somme_de_liste```( en snake case :fontawesome-regular-face-laugh-wink:
);
  - prend en **paramètre** la variable ```une_liste``` qui doit être bien entendu une liste!
  - **retourne** une valeur égale à la somme des éléments de la liste.



!!! question " Comment on utilise cette fonction?"
    C'est simple! On exécute le script qui contient cette fonction: elle est alors ajoutée aux outils disponibles. On peut l'utiliser dans le script ou dans la console...
    ```py
    >>> l1 = [1, 2, 3]
    >>> somme_de_liste(l1)
    >>> 6 #1 + 2 + 3
    ```
Dans la console, l'appel de la fonction sur la liste ```l1```  retourne la valeur 6!
!!! warning "Toute votre attention!"
    Il ne faut pas confondre le paramètre ```une_liste``` de la fonction qui est une **paramètre** de la fonction et la variable ```globale``` ```l1``` qui est une liste : lorsqu'on applique la fonction ```somme_de_liste``` à la liste ```l1```, le paramètre ```une_liste``` pointe alors vers l'objet ```l1``` et prend donc sa valeur! Le site Python Tutor permet de visualiser ce pointage.


<figure markdown>
  ![](img/param_fct_tutor.png){ width="800" }
  <figcaption> Visualisation sur Python Tutor</figcaption>
</figure>


La notion de variable **locale** ou **globale** n'est pas indispensable ici. Mais elle permet de comprendre certaines erreurs. 

Lorsqu'on programme avec des fonctions Python, on utilise un **paradigme fonctionnel** mais le développement de cette notion est réservée aux experts...

!!! quote "Une fonction, c'est quoi alors?"
    Une fonction au sens informatique , est un ensemble d'instructions regroupées en un seul nom qui prend en entrée un ou plusieurs paramètres et qui retourne une valeur (ou autre chose...)

L'avantage d'utiliser des fonctions est la **factorisation** des programmes! On enchaîne les fonctions sur une entrée pour obtenir le résultat attendu. Un exemple!

!!! exemple "Double effet"
    Utilisons des fonctions Python pour écrire des fonctions mathématiques.
    ```py
    def carre(x):
        return x**2
    def mulplicateur_par_4(x):
        return 4*x
    def additionneur_5(x):
        return x + 5
    ```

Recopiez ces instructions dans la zone de script de Thonny puis exécutez-le. Vous verrez apparaître quelque chose d'incroyable! En effet, il ne se passe rien, du moins en apparence!

!!! tip "Exécution des fonctions"
    Lorsque vous écrivez, définissez des fonctions Python et que vous les exécutez, il ne se passe rien tant que vous ne les utilisez pas!!!

En effet, après avoir exécuter le script tapez dans la console ``carre(10)`` ou ``multiplicateur_par_4(3)`` ou encore ``additionneur_5(37)`` et vous verrez alors le résultat des opérations.

Un des avantages des fonctions, c'est de pouvoir les enchaîner!

!!! exo "Utiliser des fonctions Python"
    === "Exercice"
        1. Que vaut l'instruction ``carre(multiplicateur_par_4(additionneur_5(1)))``?
        2. Et que vaut l'instruction ``multiplicateur_par_4(additionneur_5(carre(3)))``?
        3. Calculer $4\times(5 + 27)^2$ avec ces fonctions python et seulement elles!
    === "Correction"
        1. ``576``. On additionne d'abord 5 à 1, on obtient 6, qu'on multiplie par 4, ça fait 24 et on élève au carré pour obtenir 576...
        2. ``56``. On élève d'abor 3 au carré: ça fait 9. Puis on additionne 5 pour obtenir 14 qu'on multiplie enfin par 4...
        3. ``multiplicateur_par_4(carre(additionneur_5(27)))``


Les fonctions proposées ici sont un peu restrictives, au sens où elle ne permettent pas d'autres opérations de somme ou de multiplications.


        
<h2 class = "python"><span class="fa-3x">
  <i class="fa-solid fa-marker fa-flip fa-xs" style="--fa-animation-duration: 5s;" ></i>
  
</span> Exercices corrigés</h2>

!!! exo "Suite arithmétique"
    === "Exercice"
        Une suite arithmétique est parfaitement définie avec son premier terme $u_0$ et sa raison $r$.

        1. Définir la fonction python ``generateur_arithm`` qui prend en paramètre trois nombres ``u``,``r`` et ``n`` et qui retourne le terme de la suite arithmétique de premier terme ``u``  de raison ``r`` situé au rang ``n``. Par exemple, ``generateur_arithm(1, 2, 10)`` donnerait ``21``.

        2. Définir maintenant la fonction python ``somme_arithmetique`` qui calcule la somme des ``n`` premiers termes de cette suite arithmétique.
    === "Correction"
        1. On s'appuie sur les connaissances des suites, définies de façon récurrente ou explicite.
        ```py
        def generateur_arithm(u, r, n)):
            for i in range(n):
                u = u + r
            return u
        ```
        ou simplement:
        ```py
        def generateur_arithm(u, r, n)):
            return u + n*r
        ```
        2. 
        ```py
        def somme_arithmetique(u, r, n)):
            s = 0
            for i in range(n):
                s = s + u
                u = u + r
            return s
        ```
        

!!! exo "Moyenne de deux nombres"
    === "Exercice"
        1. Deux nombres étant donnés, définir la fonction ``moyenne_deux_nombres`` qui prend en paramètres les deux nombres et retourne leur 
        moyenne.
        2. Calculer la moyenne de ``27`` et ``89`` avec cette fonction.
    === "Correction"
        ```py
        def moyenne_deux_nombres(a, b):
            moy = (a + b) / 2
            return moy
        print(moyenne_deux_nombres(27, 89)) # la fonction n'affiche pas, il faut donc utiliser print pour afficher son retour
        ```

!!! exo "Moyenne de plusieurs nombres"
    === "Exercice"
        1. Des nombres étant donnés, définir la fonction ``moyenne_liste_nombres`` qui prend en paramètres la **liste** de ces nombres et retourne leur moyenne.
        2. Calculer la moyenne des nombres ``[10, 12, 8, 4, 18]`` avec cette fonction.
    === "Correction"
        On calcule d'abord la somme des éléments de cette liste qu'on divise ensuite par leur nombre.
        ```py
        def moyenne_liste_nombres(liste_nombres):
            s = 0
            for i in range(len(liste_nombres)):
                s = s + liste_nombres[i]
            return s/len(liste_nombres)
        ```
        puis 
        ```py
        print(moyenne_liste_nombres([10, 12, 8, 4, 18])) 
        ```
        ou
        ```py
        L = [10, 12, 8, 4, 18]
        print(moyenne_liste_nombres(L)) 
        ```
        
!!! exo "Moyenne pondérée de plusieurs nombres"
    === "Exercice"
        1. Des notes et leurs coefficients étant donnés, compléter la fonction ``moyenne_ponderees_nombres`` qui prend en paramètres la **liste** de ces notes et la liste de leurs coefficients et retourne leur moyenne.
        ```py
        def moyenne_ponderees_nombres(liste_nombres, liste_coefficients):
            somme_des_notes = 0
            somme_coefficient = 0
            for i in range(len(liste_nombres)):
                somme_des_notes = somme_des_notes + liste_nombres[i]*liste_coefficients[i]
                somme_coefficient = somme_coefficient + liste_coefficients[i]

            return somme_........./somme_..............
        ```
        2. Calculer la moyenne des nombres ``[10, 12, 8, 4, 18]`` avec les coefficients ``[1, 4, 2, 4, 1]``  avec cette fonction.
    === "Correction"
        On calcule deux sommes: somme des notes coefficientées et somme des coefficients
        ```py
        ```py
        def moyenne_ponderees_nombres(liste_nombres, liste_coefficients):
            somme_des_notes = 0
            somme_coefficient = 0
            for i in range(len(liste_nombres)):
                somme_des_notes = somme_des_notes + liste_nombres[i]*liste_coefficients[i]
                somme_coefficient = somme_coefficient + liste_coefficients[i]
            return somme_des_notes/somme_coefficient
        ```
        ```
        puis 
        ```py
        print(moyenne_liste_nombres([10, 12, 8, 4, 18])) 
        ```
        ou
        ```py
        L = [10, 12, 8, 4, 18]
        print(moyenne_liste_nombres(L)) 
        ```
        


      
<h2 class = "python"><span class="fa-3x">
  <i class="fa-solid fa-wrench fa-flip fa-xs" style="--fa-animation-duration: 5s;" ></i>
  
</span> Exercices</h2>

!!! exo "Exercice:"
      1. Recopier les fonctions suivantes:
      ```py
      a = 10
      b = -6
      def somme(x, y):
          return x + y
      def prod(x, y):
          return x * y
      def puissance(x, n):
          return x**n
      ```
      2. Avec ces fonctions, calculer:

          - le produit de a par b
          - la somme de a et 3
          - le produit de 15 par 148
          - $16^5$
          - le produit de la somme de a et b avec a
          - la puissance 4 de la somme du produit de b par 3 avec a.

!!! exo "Exercice:"
      On considère la fonction  ```mystere``` suivante:
      ```py
      from random import randint
      def mystere(nbre):
          L = []
          for i in range(nbre)
              L.append(randint(0, 6))
          return L
      ```

      1. Que fait cette fonction?
      2. Testez-là.
      3. Modifiez cette fonction pour que les nombres obtenus soient compris entre 0 et 20.
      4. Modifiez cette fonction pour que les nombres aléatoires soient compris entre 0 et ```N```, où ```N``` est passé en paramètre de la fonction.
      5. Créer une fonction ```moyenne_liste``` qui prend en paramètre une liste ```L``` et renvoie la moyenne des éléments de ```L```.
      6. Que donnera alors l'instruction ```moyenne_liste(mystere(10))```? 


!!! exo  "Exercice:"
      1. Écrire une fonction ```extreme``` qui prend en paramètre une liste et qui retourne la somme du premier et dernier élément de la liste
      2. Écrire une fonction ```deplace``` qui prend en paramètres une liste et un entier ```n``` plus petit que la longueur de la liste: cette fonction échange le premier élément de la liste avec celui situé au rang ```n```.





