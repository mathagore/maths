# Faire des graphiques avec Python

Pourquoi faire des graphiques? C'est une habitude pour visualiser une quantité de données:

- en physique, on réalise une expérience et on obtient des données dans un tableau que l'on exploite pour faire un graphique. 
- en maths, pour déterminer la courbe représentative d'une fonction $f$.
- en informatique, pour observer par exemple la répartition des niveaux de gris dans une image en noir et blanc.
- ...

Dans tous les cas, on utilisera la bibliothèque ```matplotlib``` qui offre tout un tas de possibilités pour faire un graphique.

!!! tip "La méthode générale!"
    Pour faire un joli graphique, il faut en général, une liste de valeurs pour les abscisses et une liste de **même taille** pour les ordonnées!

Tous le reste ne sert qu'à illustrer le graphique avec des graduations, des légendes, ...

## À partir d'un tableau de nombres:

Dès la classe de sixième, vous avez pris l'habitude de construire dans un repère cartésien du plan, des points dont les coordonnées sont données par un tableau:


|x|0|1|3|4|6|
|-|-|-|-|-|-|
|y|0|4|1|2|4|


et le code python qui permet de représenter **soit** le nuage de point **soit** la courbe passant par ces points:

!!! example "Tracés simples"

    === "Nuage de points"

        ```py
        import matplotlib.pyplot as plt
        X = [0, 1, 3, 4, 6]
        Y = [0, 4, 1, 2, 4]
        plt.scatter(X, Y)#scatter pour les points seulement
        plt.show() # affiche la figure à l'écran
        ```
    
    === "Interpolation par une courbe"

        ```py
        import matplotlib.pyplot as plt
        X = [0, 1, 3, 4, 6]
        Y = [0, 4, 1, 2, 4]
        plt.plot(X, Y)#plot pour courbe
        plt.show() # affiche la figure à l'écran
        ```
Vous devriez obtenir ceci:

<center>
![](<img/graphique1.png>){width=40%}
![](<img/graphique2.png>){width=40%}
</center>

On peut ajouter tout un tas de truc pour rendre le graphique encore plus joli. Des exemples sont proposés ci-dessous.
## À partir d'une fonction mathématique:

Souvent, il faut tracer la courbe représentative d'une fonction mathématique.

!!! example "La courbe représentative d'une fonction mathématique"
    Rien de plus simple! Il faut:

    - une fonction $f$ définie sur un intervalle $I$
    - une liste en abscisse
    - une liste en ordonnée

Voici le code à adapter selon les besoins...

??? exemple "Pour tracer une courbe représentative..."
    ```py 
    import numpy as np
    import math
    import matplotlib.pyplot as plt
    #des objets pour tracer...
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    #valeurs max et min des axes
    x_min = 0
    x_max = 0.5
    y_min = 0
    y_max = 2
    #la fonction 
    def f(x):
        return 1/np.sqrt(1.25 - np.cos(2*np.pi*x))
    #la liste en abscisses   
    X = np.arange(start = x_min, stop = x_max , step = 0.01)
    #delimitation des axes
    plt.xlim(x_min,x_max)
    plt.ylim(y_min,y_max)
    #trace de la courbe
    plt.plot(X, f(X),'r')
    plt.xlabel(r"x")
    plt.ylabel(r"f(x)")
    plt.title(r"Courbe")
    plt.show()
    ```

        
Par exemple, voici le code ```python``` pour construire la courbe représentative des fonctions $\cos$ et $\sin$ sur l'intervalle $I=[0, 2\pi]$.

??? exemple "Fonctions trigonométriques"
    ```py
    ####### import bibliothèques ###############
    import numpy as np
    import math
    import matplotlib.pyplot as plt
    ####### une figure et des axes ############
    fig, ax = plt.subplots()
    ###### valeurs extrêmes en X et Y #########
    x_min = 0
    x_max = 2*np.pi
    y_min = -1 #valeur minimale du cos et sin
    y_max = 1 #valeur maximale du cos et sin
    ##### fonction cos et sin #################
    def lesinus(x):
        return np.sin(x)
    def lecosinus(x):
        return np.cos(x)
    ##### génération de la liste des abscisses
    X = np.arange(start = x_min, stop = x_max, step = 0.01)
    #### limite horizontale du graphique ######
    plt.xlim(x_min, x_max)
    #### limite verticale du graphique ########
    plt.ylim(y_min, y_max)
    ###### pour les grilles ###################
    grid_x_ticks = np.arange(x_min, x_max, 0.5)
    grid_y_ticks = np.arange(y_min, y_max, 0.25)
    ###### pour les graduations
    ax.set_xticks(grid_x_ticks, minor= False)
    ax.set_yticks(grid_y_ticks , minor= False)
    #### pour l'affichage de la grille ########
    ax.grid(which='both')
    ### construction des deux courbes
    # X est la première liste et lesinus(X) est la deuxième
    plt.plot(X, lesinus(X), color ='red', label ='cos')
    plt.plot(X, lecosinus(X), color = 'green', label ='sin')
    ##### pour la légende #####################
    plt.xlabel(r"$x$ ")#syntaxe latex
    plt.ylabel(r" $\sin(x)$ et $\cos(x)$")#syntaxe latex
    plt.legend()
    plt.title(r"Fonctions trigonométriques ")
    plt.show()
    ```

On obtient le graphique suivant:

<center>
![Sin et Cos](<img/fct_trigo.png>)
</center>