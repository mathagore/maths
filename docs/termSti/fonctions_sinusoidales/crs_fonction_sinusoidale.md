Les **fonctions sinusoïdales** jouent un rôle important dans le traitement du signal.
L'étude de leur propriété est incontournable.




# Le cours:


<div class="centre">
<iframe 
src="./crs_fonction_sinus.pdf"
width="1000" height="500" 
frameborder="0" 
allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
</iframe>
</div>


# Les exercices:

[Fiche n°1: Exercices sur les transformations trigonométriques](./exo_sinusoide.pdf){ .md-button target="_blank" rel="noopener" }





