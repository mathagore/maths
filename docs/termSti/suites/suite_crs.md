En classe de première, vous avez appris la notion de suites numériques et particulièrement les suites **arithmétiques** et **géométriques**. En classe de Terminale, on complète cette étude par les formes explicites de ces suites et comment peut-on les manipuler avec des routines Python.


# Le cours:


<div class="centre">
<iframe 
src="./Crs_suitesAG.pdf"
width="1000" height="500" 
frameborder="0" 
allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
</iframe>
</div>


# Les exercices:

[Fiche n°1: Manipuler des suites arithmétiques et géométriques](./Exo_Suites_1.pdf){ .md-button target="_blank" rel="noopener" }

[Fiche n°2: Modéliser avec des suites arithmétiques et géométriques](./Exo_Suites_2.pdf){ .md-button target="_blank" rel="noopener" }

[Fiche n°3: Croissance linéaire ou exponentielle?](./Exo_Suites_3.pdf){ .md-button target="_blank" rel="noopener" }

[Fiche n°4: Python et les suites arithmétiques et géométriques](./suite_python.pdf){ .md-button target="_blank" rel="noopener" }


{{ multi_qcm(
    ["Compléter : $2^3=$", ["9", "8", "Je ne sais pas", "6"],[2],],
    multi = False,
    qcm_title = "Puissances",
    shuffle = True,
) }}