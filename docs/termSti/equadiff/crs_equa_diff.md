Les équations différentielles modélisent des situations issues de la physique dans de nombreux domaines: électricité, mouvement, propagation de la chaleur,...

Le cours de mathématiques donne les méthodes pour résoudre les équations différentielles du premier ordre à coefficients constants.



# Le cours:


<div class="centre">
<iframe 
src="./Crs_Equa_Diff.pdf"
width="1000" height="500" 
frameborder="0" 
allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
</iframe>
</div>


# Les exercices:

[Fiche n°1: Exercices applications directes du cours](./exo_equa_diff.pdf){ .md-button target="_blank" rel="noopener" }

[Correction de la fiche: Exercices applications directes du cours](./correction_equa_diff.pdf){ .md-button target="_blank" rel="noopener" }

[Fiche n°2: Exercices approfondis](./exo_equa_diff_bis.pdf){ .md-button target="_blank" rel="noopener" }

[Fiche n°3: La méthode d'Euler](./methode_Euler.pdf){ .md-button target="_blank" rel="noopener" }