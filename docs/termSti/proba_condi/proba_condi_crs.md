Le calcul de probabilités conditionnelles vient compléter l'étude des situations probabilistes rencontrées depuis les classes de troisième. Rappelons que le calcul de probabilité se résume au calcul du rapport de la partie (recensant les cas favorables) au tout (recensant les cas possibles).

Ce cours permet aussi de réviser les méthodes déjà rencontrées dans les classes antérieures.

# L'activité préparatoire

[Activité n°1: Modéliser par des arbres, des tableaux](./act_preparatoire_proba_condi.pdf){ .md-button target="_blank" rel="noopener" }


# Le cours:


<div class="centre">
<iframe 
src="./Crs_proba_condi.pdf"
width="1000" height="500" 
frameborder="0" 
allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
</iframe>
</div>


# Les exercices:

[Fiche n°1: Calcul de probabilités conditionnelles avec les formules du cours](./exo_proba_condi.pdf){ .md-button target="_blank" rel="noopener" }

[Fiche n°2: Exercices de synthèses ](./exo_proba_condi_synthese.pdf){ .md-button target="_blank" rel="noopener" }


