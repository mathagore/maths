Les fonctions exponentielles de base quelconque ont déjà été rencontrées dans l'étude du logarithme décimal. Il s'agit ici d'étudier la fonction exponentielle de base $e$.


Elle possède la propriété remarquable suivante:

<center>

$f(x)=f'(x)$ avec $f(0)=1$

</center>

prélude à la résolution d'équations différentielles...




# Le cours:


<div class="centre">
<iframe 
src="./Crs_fonct_expo.pdf"
width="1000" height="500" 
frameborder="0" 
allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
</iframe>
</div>


# Les exercices:

[Fiche n°1: Exercices simples sur la fonction exponentielle et son logarithme](./exo_fonction_expo_base_e.pdf){ .md-button target="_blank" rel="noopener" }




