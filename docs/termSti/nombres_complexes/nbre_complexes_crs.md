Les **nombres complexes** portent un nom bien malheureux. D'ailleurs c'est davantage le qualicatif d'**imaginaires** que l'histoire leurs avait donné...

Un nombre complexe (ou imaginaire!), c'est la donnée de deux nombres réels ayant des propriétés calculatoires que vous trouverez dans le cours suivant...

De nombreuses applications, dans le domaine de l'électricité, légitiment l'utilisation de ces nombres peu communs.

# L'activité préparatoire

[Rappel : Retour sur les formes algébriques des nombres complexes](./Crs_Nombres_complexes.pdf){ .md-button target="_blank" rel="noopener" }


# Le cours:


<div class="centre">
<iframe 
src="./Crs_Nombres_complexesFormeGeometrique.pdf"
width="1000" height="500" 
frameborder="0" 
allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
</iframe>
</div>


# Les exercices:

[Fiche n°1: Travail sur les formes trigonométriques](./exo_synthese_forme_trigo.pdf){ .md-button target="_blank" rel="noopener" }

[Fiche n°2: Passage à la forme exponentielle ](./exo_forme_expo_complexe.pdf){ .md-button target="_blank" rel="noopener" }

[Fiche n°3: Exercices de synthèse](./exo_nbres_complexes.pdf){ .md-button target="_blank" rel="noopener" }





