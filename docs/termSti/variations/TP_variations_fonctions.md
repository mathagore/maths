<link rel ="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.7.2/css/all.min.css">

<h2 class = "python"><span class="fa-3x">
  <i class="fa-solid fa-user-graduate fa-flip fa-sm" style="--fa-animation-duration: 5s;"></i>
  
</span> Étudier les variations des fonctions sur un intervalle I</h2>


Ce document donne toutes les informations au niveau Terminale STI pour étudier les variations d'une fonction sur un intervalle $I$.

Ce travail doit être fait en total autonomie. Vous trouverez des:

- cours qui présente des contenus théoriques et des méthodes
- des exercices corrigés types
- des vidéos qui donnent plus de précision sur les méthodes de dérivation.
- des exercices à faire seul, sans correction.


## Précisons le vocabulaire

!!! question "Mais qu'est ce que signifie étudier les variations d'une fonction?"
    Déterminer les variations d'une fonction $f$ sur un intervalle $I$, c'est dire si elle est **croissante** ,**décroissante** voire **constante** sur $I$.

En général, depuis la classe de seconde, on dresse le **tableau** de variations de la fonction $f$ sur l'intervalle $I$ pour **recenser** toutes les variations de $f$ sur $I$. Un exemple:

<center>
![image](tab_variations.png){width=80% }
</center>

On rappelle aussi les définitions suivantes:

!!! info "Croissance et décroissance"
    Une fonction $f$ est **croissante** sur un intervalle $I$ si toute **augmentation** de la variable $x$ entraîne aussi une **augmentation** des images $f(x)$.

    Une fonction $f$ est **décroissante** sur un intervalle $I$ si toute **augmentation** de la variable $x$ entraîne une diminution des images $f(x)$.

Comprenez qu'une fonction établit le lien entre deux grandeurs: $x$, la variable  et $f(x)$ son image. Si une augmentation de $x$ implique une augmentation de $f(x)$ alors $f$ est dite croissante sur l'intervalle $I$ auquel appartient $x$.

!!! question "Mais à quoi ça peut servir d'étudier les variations d'une fonction?"
    L'étude des variations d'une fonction permet de déterminer tout **minimum** ou **maximum** de cette fonction sur l'intervalle $I$.

Des exercices d'**optimisation** mettront en relief cette proriété. Patience...


# Des cas connus

### Les fonctions affines

L'étude exhaustive des fonctions **affines** en classe de seconde donne déjà des premiers résultats.

Une fonction affine est une fonction $f$ qui peut s'écrire $f(x)=ax+b$ où $a$(le coefficient directeur) et $b$ (l'ordonnée à l'origine) sont deux nombres réels qui définissent parfaitement cette fonction.
    
!!! tip "Cas des fonctions affines"
    Sur n'importe quel intervalle de réels $I$, une fonction affine est:

    - croissante si $a>0$
    - décroissante si $a<0$
    - constante si $a=0$
    
De toute façon, une fonction affine est une fonction **monotone** car elle ne change pas de variations.

!!! exo "Des exemples"
    === "Exercices"
        Déterminer les variations des fonctions affines suivantes:

        1. $f(x)= 2x-1$
        2. $g(t)= -0.1t+5$
        3. $h(x)= \dfrac{5x-256}{2}$
        4. $R(t)= 175-2t$
        5. $Q(t) = 500$
    === "Correction"
        Dans chaque cas, il faut et il suffit de trouver le signe de $a$, le coefficient directeur, de chaque fonction affine:

        1. $f(x)= 2x-1$, $a=2$ et $f$ est croissante sur $\mathbb{R}$
        2. $g(t)= -0.1t+5$, $a=-0.1$ et $g$ est décroissante sur $\mathbb{R}$
        3. $h(x)= \dfrac{5x-256}{2}$, $a=\dfrac{5}{2}$ et $h$ est croissante sur $\mathbb{R}$
        4. $R(t)= 175-2t$, $a=-2$ et $R$ est décroissante sur $\mathbb{R}$
        5. $Q(t) = 500$, $a=0$ et $Q$ est constante sur $\mathbb{R}$

Dans les cas précédents où les fonctions sont monotones, il n'est pas toujours nécessaires de donner le tableau de variations de $f$.

!!! warning "Confusion habituelle"
    Ne pas confondre **tableau de variations** et **tableau de signes** d'une fonction sur un intervalle $I$.

La confusion est habituelle chez les élèves...

### Les fonctions carrés

La fonction carré puis plus généralement, les fonctions **polynômes du second degré**(ou plus simplement **trinôme**) sont introduites en classe de première.

!!! info "Les trinômes"
    Les trinômes sont des fonctions dont les courbes représentatives sont des **paraboles**.

Ces fonctions **modélisent** par exemple, les trajectoires des objets soumis à une gravitation...

À la différence des fonctions affines, les fonctions du second degré vont donc présenter un **minimum** ou un **maximum** selon les cas.

!!! question "C'est un quoi, un minimum, un maximum???"
    Une fonction $f$ admet un **minimum** en $x_0$ si pour tout $x$ dans $I$, toutes les valeurs de $f(x)$ sont plus grandes que $f(x_0)$, qui est alors la **valeur minimale** de $f$.

    Une fonction $f$ admet un **maximum** en $x_0$ si pour tout $x$ dans $I$, toutes les valeurs de $f(x)$ sont plus petites que $f(x_0)$, qui est alors la **valeur maximale** de $f$.


Graphiquement, $x_0$ et $f(x_0)$ seraient alors les coordonnées du sommet de la parabole représentative de $f$.

Toute fonction du second degré peut s'écrire sous la forme $f(x)= ax^2+bx+c$ où $a$,$b$ comme $c$ sont trois réels qui définissent parfaitement cette fonction. On a alors le résultat suivant:


!!! info "Variations des fonctions du second degré"
    Une fonction trinôme est:

    - croissante puis décroissante sur $\mathbb{R}$ si $a<0$
    - décroissante puis croissante sur $\mathbb{R}$ si $a>0$

Le cas $a=0$ n'existe pas sinon il n'y aurait plus de fonction de second degré... En d'autres termes, une fonction trinôme admet un **maximum** si $a<0$ et un **minimum** si $a>0$.

!!! info "Coordonnées du sommet $S$ de la parabole"
    Dans tous les cas, une fonction trinôme admet son minimum ou son maximum en $x=-\dfrac{b}{2a}$ et vaut alors $f(-\dfrac{b}{2a})$.


!!! exo "Des exemples"
    === "Exercices"
        Dans chaque cas, dire si la fonction admet un minimum ou un maximum puis préciser la valeur de cet extremum et en quelle valeur de $x$ il est atteint.

        1. $f(x)=5x^2-10x+20$
        2. $g(t)=-0.25t^2+6t+1$
        3. $B(q)= -0.5q^2+7.4q-8.16$

    === "Correction"
        Il suffit d'identifier le signe de $a$ dans chaque cas. Rappelons que $a$ est le nom donné au coefficient qui multiplie le carré de la variable. Les valeurs extrêmes sont données par la formule $f(-\dfrac{b}{2a})$ en $-\dfrac{b}{2a}$ si $f$ est la fonction...

        1. $a=5$ donc $f$ admet un **minimum** en $x=-\dfrac{b}{2a}=-\dfrac{-10}{2\times 5}=1$ et ce minimum vaut $f(1)=5-10+20=15$
        1. $a=-0.25$ donc $g$ admet un **maximum** en $t=-\dfrac{b}{2a}=-\dfrac{6}{2\times (-0.25)}=12$ et ce minimum vaut $g(12)=-0.25\times12^2+6\times12+1=37$
        1. $a=-0.5$ donc $B$ admet un **maximum** en $q=-\dfrac{b}{2a}=-\dfrac{7.4}{2\times (-0.5)}=7.4$ et ce minimum vaut $B(7.4)=19.22$


Nous pourrions continuer à évoquer les variations des fonctions de référence en multipliant les cas rencontrés.

Mais je préfère maintenant me concentrer sur la **méthode générale** pour déterminer les variations d'une fonction sur un intervalle $I$. Celle amorcée en classe de première et fortement utilisée en classe de terminale. C'est l'objet du prochain paragraphe...

# Une méthode globale
Soit $f$ une fonction **quelconque** dont on veut déterminer les variations sur un intervalle $I$.

Nous allons utiliser la fonction **dérivée** de $f$ pour étudier les variations de $f$ sur $I$. 

!!! info "Fonction dérivée $f'$ de $f$"
    La fonction dérivée $f'$ de $f$ est la fonction qui génère les nombres dérivés de $f$.

Le **nombre dérivé** est lié à la variation **locale** des fonctions: il donne le taux d'accroissement d'une fonction autour d'une valeur, d'un point...

!!! note "Déterminer la dérivée $f'$ d'une fonction $f$"
    Il existe de nombreuses techniques largement éprouvées pour déterminer la fonction dérivée d'une fonction $f$. Pour les cas simples, regargez [ici](https://mathagore.forge.apps.education.fr/maths/termSti/derivation/rappel_derivation.html)

L'étude du signe de la dérivée donne alors une variation **globale** de la fonction sur tout un intervalle. Précisément:

!!! tip "Lien entre signe de $f'$ et variations de $f$ sur un intervalle $I$"
    Si la fonction dérivée $f'$ est **négative** pour tout $x$ dans l'intervalle $I$ alors $f$ est **décroissante** sur $I$ et réciproquement.

    Si la fonction dérivée $f'$ est **positive** pour tout $x$ dans l'intervalle $I$ alors $f$ est **croissante** sur $I$ et réciproquement.

Cette propriété est à la base de la méthode globale d'étude des variations d'une fonction $f$.

!!! tip "Méthode globale pour étudier les variations d'une fonction sur un intervalle $I$"

    1. On détermine la fonction dérivée $f'$ de $f$.
    2. On étudie le signe de $f'$ sur $I$.
    3. On dresse le tableau de variations de $f$ sur $I$.

Cette méthode fonctionne dans tous les cas:

- pour des fonctions simples, comme les fonctions affines ou les fonctions du second degré(dont on connaît a priori les variations...)
- pour des fonctions plus complexes, contenant des logarithmes ou des exponentielles.

Nous allons donc illustrer sur des exemples d'abord simple l'utilisation de cette méthode avant de proposer des exemples plus complexes.

# Des méthodes

## Déterminer la dérivée d'une fonction(cas plus évolués)
### Avec exponentielle

### Avec logarithme

## Déterminer le signe d'une expression
### Par calculs
### Par lecture graphique

## Étudier les variations d'une fonction $f$ sur un intervalle


