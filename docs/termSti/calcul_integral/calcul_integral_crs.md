Le calcul intégral vient en complément du calcul de dérivée amorcé en classe de première. Associé à un calcul de l'aire d'un domaine, il permet de généraliser la notion de somme finie et infinie apparue dans les suites numériques.

Il est important, avant de débuter le calcul intégral de connaître la notion de **primitives** de fonctions continues...
# L'activité préparatoire

[Activité n°1: Approcher l'aire d'un domaine par l'aire d'un polygone](./act_aire_sous_une_courbe.pdf){ .md-button target="_blank" rel="noopener" }


# Le cours:


<div class="centre">
<iframe 
src="./Crs_calcul_integral.pdf"
width="1000" height="500" 
frameborder="0" 
allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
</iframe>
</div>


# Les exercices:

[Fiche n°1: Déterminer voire calculer la valeur d'une intégrale](./exo_integration.pdf){ .md-button target="_blank" rel="noopener" }

[Fiche n°2: Valeur moyenne et calcul d'aires ](./exo_integration_application.pdf){ .md-button target="_blank" rel="noopener" }

# Compléments:

[Activité n°2: Les méthodes d'approximations ](./algorithme_approximation.pdf){ .md-button target="_blank" rel="noopener" }


