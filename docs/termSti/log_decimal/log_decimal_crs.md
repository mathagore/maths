Le logarithme en base 2 est naturel chez les informaticiens: c'est l'outil mathématique qui aux puissances de 2 successives(1,2,4,8,...) associe les valeurs 0,1,2,3 indiquant la puissance.

Les chimistes comme les acousticiens sont plus habitués au logarithme en base 10, appelé **logarithme décimal**...

On se propose ici d'étudier les propriétés des fonctions exponentielles et de leurs logarithmes auxquels elles ont intimements liées...


# Le cours:


<div class="centre">
<iframe 
src="./Crs_logdecimal.pdf"
width="1000" height="500" 
frameborder="0" 
allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
</iframe>
</div>


# Les exercices:

[Fiche n°1: Utiliser les propriétés du logarithme pour résoudre des équations](./exo_logarithme_decimal.pdf){ .md-button target="_blank" rel="noopener" }

[Fiche n°2: Modéliser avec des fonctions exponentielles de base $a$](./exo_fonction_expo_base_a.pdf){ .md-button target="_blank" rel="noopener" }

# Compléments:

[Activité n°1: Activité sur le taux moyen ](./act_taux_moyen.pdf){ .md-button target="_blank" rel="noopener" }


