En classe de première, vous avez vu la notion de **nombre dérivé** puis de **fonctions dérivées**. 
Ce petit document en rappelle les principales connaissances.


# Le cours:


![Carte mentale](carte_mentale_derivation.png){width = 40%}

# Les exercices:

[Fiche n°1: Déterminer un nombre dérivé](./Tp_determiner_un_nombre_derive.pdf){ .md-button target="_blank" rel="noopener" }

[Fiche n°2: Déterminer une fonction dérivée](./Tp_determiner_fct_derive.pdf){ .md-button target="_blank" rel="noopener" }


# Des trucs en plus
   
    
!!! exo "Déterminer la dérivée de fonctions simples"

    === "Enoncé"

        1. $f(x)=2x-3$
        2. $f(x)=5x-1$
        3. $f(x)=6$
        4. $f(x)=4-x$
        5. $f(x)=3x^2$
        6. $f(x)=5x^3$
        7. $f(t)=\sin(t)$
        8. $f(r)=3r-2$
        9. $g(t)=\sqrt{t}$
        10. $f(x)=8x^2-3x+10$


    === "Solution"

        1. $f'(x)=2$
        2. $f'(x)=5$
        3. $f'(x)=0$
        4. $f'(x)=-1$
        5. $f'(x)=6x$
        6. $f'(x)=15x^2$
        7. $f'(t)=\cos(t)$
        8. $f'(r)=3$
        9. $g'(t)=\dfrac{1}{2 \sqrt{t}}$
        10. $f'(x)=16x-3$

!!! exo "Pour les meilleurs! Déterminer la fonction dérivée des fonctions proposées."

    === "Enoncé"

        1. $f(x)=t$
        2. $f(x)=xt-5$
        3. $f(t)=xt-5$
        4. $f(x)=4-x^t$
        


    === "Solution"

        1. $f'(x)=0$
        2. $f'(x)=t$
        3. $f'(x)=x$
        4. $f'(x)=-tx^{t-1}$

!!! exo "Déterminer des fonctions dérivées"
    Dans un cadre plus général où les fonctions proposées ne sont pas que des fonctions de références, déterminer les dérivées des fonctions suivantes:
    === "Enoncé"

        1. $f(x)=5x^2-4\cos(x)$
        2. $f(x)=10x^9-x^8-2x^6+7x^5-x^3+4$
        3. $f(t)=t \sin(t)$
        4. $f(x)=(x^2+1)(5x+2)$
        5. $f(x)=\dfrac{x^2+1}{5x+2}$
        4. $f(x)=(2-x)(x+5)$
        5. $f(x)=\dfrac{2-x}{x+5}$
        


    === "Solution"

        1. $f'(x)=10x+4\sin(x)$
        2. $f'(x)=90x^8-8x^7-12x^5+35x^4-3x^2$
        3. $f'(t)=\sin(t)+t \cos(t)$($f$ est un produit de ... donc choisir la bonne formule!)
        4. $f'(x)=2x(5x+2)+(x^2+1)\times 5=15x^2+4x+5$($f$ est un produit de ... donc choisir la bonne formule!)
        5. $f'(x)=\dfrac{2x(5x+2)-(x^2+1)\times 5}{(5x+2)^2}=\dfrac{5x^2+4x-5}{(5x+2)^2}$
        6. $f'(x)=-1\times(x+5)+(2-x)\times 1=-2x-3$
        7. $f'(x)=\dfrac{-1\times (x+5)-(2-x)\times 1}{(x+5)^2}=\dfrac{-7}{(x+5)^2}$


